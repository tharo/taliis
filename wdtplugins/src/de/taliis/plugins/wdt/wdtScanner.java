package de.taliis.plugins.wdt;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URLClassLoader;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.MenuElement;

import starlight.taliis.core.files.wdt;

import de.taliis.editor.configMananger;
import de.taliis.editor.fileMananger;
import de.taliis.editor.openedFile;
import de.taliis.editor.plugin.Plugin;
import de.taliis.editor.plugin.eventServer;



public class wdtScanner implements Plugin, ActionListener {
	fileMananger fm;
	JMenuItem mScanner;
	JMenu menu;
	@Override
	public boolean checkDependencies() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public ImageIcon getIcon() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getPluginType() {
		// TODO Auto-generated method stub
		return Plugin.PLUGIN_TYPE_FUNCTION;
	}

	@Override
	public String[] getSupportedDataTypes() {
		// TODO Auto-generated method stub
		return new String[] { "wdt" };
	}

	@Override
	public String[] neededDependencies() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setClassLoaderRef(URLClassLoader ref) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setConfigManangerRef(configMananger ref) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEventServer(eventServer ref) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setFileManangerRef(fileMananger ref) {
		// TODO Auto-generated method stub
		fm = ref;
	}

	@Override
	public void setMenuRef(JMenuBar ref) {
		// TODO Auto-generated method stub
		// cast out edit menue
		for(MenuElement men : ref.getSubElements()) {
			if(men instanceof JMenu) {
				if(((JMenu)men).getName().contains("edit_wdt")) {
					menu = (JMenu)men;
					menu.setEnabled(true);
					break;
				}
			}
		}
		
		mScanner=new JMenuItem("Scan Folder");
		mScanner.addActionListener(this);
		menu.add(mScanner);
	}

	@Override
	public void setPluginPool(Vector<Plugin> ref) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void unload() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		openedFile os = fm.getActiveFile();
		wdt obj = null;
		if(os.obj instanceof wdt){
			obj=(wdt) os.obj;
		}
		else return;
		for(int x=0;x<64;x++){
			for(int y=0;y<64;y++){
				String name=os.f.getAbsolutePath().substring(0, os.f.getAbsolutePath().length()-4)+"_"+x+"_"+y+".adt";
				openedFile of = null;
				try {
					of = fm.openFileLocation(name);
				}
				catch(Exception e){
					
				}
				if(of!=null){
					obj.main.setValue(x, y, 1);
					fm.closeFile(of);
				}
			}
		}
		
	}
	
}