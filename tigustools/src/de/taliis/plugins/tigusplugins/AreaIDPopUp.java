package de.taliis.plugins.tigusplugins;


import java.awt.Component;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JTextField;



public class AreaIDPopUp implements ActionListener{

	public int areaid=0;
	public boolean ok = false;
	JDialog frame;
	JTextField id;
	JButton okButton = new JButton("OK");
	
	
	public void addComponentsToPane(Container pane){
    	pane.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		
	
		id = new JTextField("0");
		pane.add(id);
		
		
		c.gridx = 6;
		c.gridy = 2;
		c.gridwidth = 2;
		c.fill = GridBagConstraints.NONE;
		okButton.addActionListener(this);
		pane.add(okButton, c);
	
	}
	
	public AreaIDPopUp(Component rel){
		//Create and set up the window.
        frame = new JDialog();
        frame.setLocationRelativeTo(rel);
        frame.setTitle("Set AreaID");
        frame.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        
        // using this line make the Java VM crash at java 5 03 MAC .. o.O
        // static or nonstatic icon doesnt madder
        //frame.setIconImage(IconNew.getImage());
        frame.setModal(true);
        
        JPanel bla = new JPanel();

        
        //Set up the content pane.
        addComponentsToPane(bla/*frame.getContentPane()/**/);
        frame.setContentPane(bla);
        //Display the window.
        frame.pack();
        frame.setResizable(false);
        frame.setVisible(true);
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		if(arg0.getSource()==okButton) {
			areaid=Integer.parseInt(id.getText());
			frame.dispose();
			ok = true;
		}
	}
	
}