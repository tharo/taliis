package de.taliis.plugins.tigusplugins.mixeditor;

import java.io.InvalidClassException;

import de.taliis.editor.fileMananger;
import de.taliis.editor.openedFile;
import de.taliis.editor.plugin.Plugin;
import de.taliis.editor.plugin.PluginRequest;
import starlight.taliis.core.chunks.ChunkNotFoundException;
import starlight.taliis.core.files.dbc;


/**
 * This Loads the dbc and returns the requested value 
 * @author Bastian
 *
 */


public class dbcLoader {
	fileMananger fm;
	Plugin mpqLoader;
	
	public dbcLoader(fileMananger fm, Plugin mpqLoader){
		this.mpqLoader = mpqLoader;
		this.fm = fm;
	}
	/**
	 * 
	 * @param dbcname
	 * @param vCol valueField
	 * @param dCol displayField
	 * @param row the row/value which shall be translated
	 * @return
	 */
	public String getDBCValue_s(String dbcname,int vCol,int dCol,int row){
		String rets="";
		dbc src;
		//request (load) DBC file
		openedFile of = null;
		
		// ask the file loader
		for(openedFile af : fm.getFileList()) {
			if(af.f.getName().endsWith(dbcname)) {
				of = af;
				break;
			}
		}
		// ask the mpq loader
		if(of==null) {
			if(mpqLoader==null) return null;
			if(!(mpqLoader instanceof PluginRequest)) return null;
			PluginRequest mpql = (PluginRequest)mpqLoader;

			try {
				of = mpql.requestRessource("DBFilesClient\\" + dbcname);
			} catch (InvalidClassException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ChunkNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if(of==null) return null;
		src = (dbc)of.obj;
		// copy data
		try{
			Object t;
			int rrow = 0;
			for(int i=0;i<src.getNRecords();i++)
			if(((Number)src.getData(vCol, i)).intValue()==row){
				rrow=i;
				break;
			}
	    	//int vIndex = ((Number)t).intValue();
	    	
	    	// index in the string table
	    	t = src.getData(dCol, rrow);
	    	int strIndex = ((Number)t).intValue();
	    	
	    	rets=src.getStringByOffset(strIndex).toString();
		}catch (Exception e){
			return null;
		}
	    	return rets;
	}

	
}