package de.taliis.plugins.adt;

import java.awt.BorderLayout;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Vector;

import javax.media.opengl.GLCanvas;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPanel;


import starlight.taliis.core.files.adt;
import de.taliis.editor.configMananger;
import de.taliis.editor.fileMananger;
import de.taliis.editor.openedFile;
import de.taliis.editor.plugin.Plugin;
import de.taliis.editor.plugin.PluginView;
import de.taliis.editor.plugin.eventServer;
import de.taliis.editor.ClassPathHacker;

import de.taliis.plugins.adt.jogl3D.Tutorial2;


/**
 * Yes it is back ...
 * A new atemp of visualisizing ADT files in full 3D
 * 
 * @author ganku
 *
 */
public class adt3DView implements Plugin, PluginView {
	fileMananger fm;
	ImageIcon icon = null;
	ClassLoader classLoader;
	Vector<Plugin> pp;
	
	String dep[] = {
			"starlight.taliis.core.files.wowfile",
			"starlight.taliis.core.files.adt",
			"javax.media.opengl.GLCanvas",
			"de.taliis.plugins.adt.adtStorage"
	};
	String libs[] = {
			"libs/jogl.jar",
			"libs/gluegen-rt.jar"
		};
	
	
	public boolean checkDependencies() {
		String now = "";
		
		// load libarys
		if(!loadLibs()) return false;
		else {
			System.out.println("\t- additional 3D Lib's loaded.");
		}
		
		// check all classes
		try {
			for(String s : dep) {
				now = s;
				Class.forName(s);
			}
			return true;
		} catch(Exception e) {
			System.err.println("Class \"" + now + "\" not found.");
			return false;
		}
	}

	/**
	 * It is needed to hack the class-path again. Im not sure why,
	 * but it seems as after the plugin loading there was a reset?
	 * @return
	 */
	private boolean loadLibs() {
		// try to load our additional libarys from the
		// lib dir

		try {
			for(String filename : libs )
				ClassPathHacker.addFile(filename);
		} catch (IOException e) {
			return false;
		} 
		
		return true;
	}

	public ImageIcon getIcon() {
		return icon;
	}

	public int getPluginType() {
		return PLUGIN_TYPE_VIEW;
	}

	public String[] getSupportedDataTypes() {
		return new String[] {"adt"};
	}

	public String[] neededDependencies() {
		return dep;
	}

	public void setClassLoaderRef(URLClassLoader ref) {
		try {
			URL u = ref.getResource("icons/contrast.png");
			icon = new ImageIcon( u );
		} catch(NullPointerException e) {}
	}

	public void setConfigManangerRef(configMananger ref) { }
	public void setEventServer(eventServer ref) { }
	public void setFileManangerRef(fileMananger ref) {
		fm = ref;
	}
	public void setMenuRef(JMenuBar ref) { }



	public void unload() {
		//TODO: unload the 3D stuff (neccesary?)
	}
	
	public void setPluginPool(Vector<Plugin> ref) {
		pp = ref;
	}

	public JPanel createView() {
		// get file
		openedFile of = fm.getActiveFile();
		
		if(of==null || !(of.obj instanceof adt)) return null;
		
		// get data
		adt obj = (adt)of.obj;

		// 3D Init
		GLCanvas glScene = null;
		Tutorial2 glEvents = null;
		
		JPanel r = new JPanel(new BorderLayout());
		r.add(new JLabel ("To be written."));
		
		// handle events
		glScene = new GLCanvas();
		for(Plugin p : pp) {
			if(p.getClass().toString().endsWith("mpqFileRequest"))
				glEvents = new Tutorial2(obj, fm, p);
		}
		
		glScene.addGLEventListener(glEvents);
		
		r.addKeyListener(glEvents);
		r.addMouseListener(glEvents);
		glScene.addKeyListener(glEvents);
		glScene.addMouseListener(glEvents);
		
		r.add(glScene, BorderLayout.CENTER);

		return r;
	}
	
	public String toString() {
		return "3D View";
	}
}
