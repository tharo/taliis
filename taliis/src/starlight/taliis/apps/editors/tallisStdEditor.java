package starlight.taliis.apps.editors;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JPanel;

import starlight.taliis.helpers.fileLoader;

public class tallisStdEditor extends JPanel implements ActionListener {
	final protected String ICON_ADD = "add";
	final protected String ICON_DEL = "del";
	final protected String ICON_COPY = "page_copy";
	
	public tallisStdEditor() {}

	protected JButton makeNavigationButton(String imageName,
            String actionCommand,
            String toolTipText,
            String altText) {
		//Look for the image.
		String imgLocation = "images/icons/"
		+ imageName
		+ ".png";

		//Create and initialize the button.
		JButton button = new JButton();
		button.setActionCommand(actionCommand);
		button.setToolTipText(toolTipText);
		button.addActionListener(this);
		
		if (imageName != "" && imageName != null) {    //image found
			button.setIcon(fileLoader.createImageIcon(imgLocation));
		} 
		else {                                     //no image found
			button.setText(altText);
			System.err.println("Resource not found: " + imgLocation);
		}

		return button;
	}

	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
	}
}
