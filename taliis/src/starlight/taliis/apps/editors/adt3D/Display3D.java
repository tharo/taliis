package starlight.taliis.apps.editors.adt3D;

import java.awt.GraphicsConfiguration;
import java.awt.Panel;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.media.j3d.AmbientLight;
import javax.media.j3d.BoundingSphere;
import javax.media.j3d.BranchGroup;
import javax.media.j3d.Canvas3D;
import javax.media.j3d.DirectionalLight;
import javax.media.j3d.SceneGraphPath;
import javax.media.j3d.Shape3D;
import javax.media.j3d.Transform3D;
import javax.media.j3d.TransformGroup;

import com.sun.j3d.utils.behaviors.vp.OrbitBehavior;
import com.sun.j3d.utils.geometry.Primitive;
import com.sun.j3d.utils.geometry.Sphere;
import com.sun.j3d.utils.picking.PickCanvas;
import com.sun.j3d.utils.picking.PickResult;
import com.sun.j3d.utils.universe.SimpleUniverse;
import com.sun.j3d.utils.universe.ViewingPlatform;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.vecmath.Color3f;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector3f;

import starlight.taliis.core.files.adt;

/**
 * This class is going to handle all our 3d setup. It extends the 
 * main Java3D class for displaying the world, Canvas3D.
 *
 * @author Kevin Glass
 */
public class Display3D extends Canvas3D implements MouseListener {
    // our universe
    private SimpleUniverse universe;
    // main branchgroup to put everything in 
    private BranchGroup scene;
    // main transformationgroup for our scene
    private TransformGroup tg;
    // canvas to pickup things later
    private PickCanvas pickCanvas;
    
    // bounding sphere and its dimension
    private BoundingSphere bounds;
    private static final int BOUNDSIZE = 100;
    
    // adt file object
    private adt obj;
    
    /** 
     * Creates a new instance of Display3D 
     */
    public Display3D(adt adt_obj) {
        super(SimpleUniverse.getPreferredConfiguration());
        
        obj = adt_obj;
        
        // construct the simple universe supplying it with 
        // the canvas to render onto
        universe = new SimpleUniverse(this);
        
        // create bounds for scroling and lightning and so on
        bounds = new BoundingSphere(new Point3d(0,0,0), BOUNDSIZE);
        
        // create the base branch group in which our world
        // will be put
        scene = new BranchGroup();
      
        pickCanvas = new PickCanvas(this, scene);
        addMouseListener(this);
        
        // set focusable for mous control later
        setFocusable(true); 
        requestFocus();
        
        // setup ...
        lightScene();			// lights
        buildScene();			// things 
        setupView();			// camera pos
        orbitControls(this);	// mouse control
        setupBehaviors();
        
        // set the base branch group in the universe
        universe.addBranchGraph(scene);
    }
    
    private void setupBehaviors() {
    	BevWASD behav = new BevWASD();
    	behav.setTransformGroup(tg);
    	scene.addChild(behav);
    }
    
    private void lightScene() {
    	Color3f white = new Color3f(1.0f, 1.0f, 1.0f);
    	
    	// Set up the ambient light
        AmbientLight ambientLightNode = new AmbientLight(white);
        ambientLightNode.setInfluencingBounds(bounds);
        scene.addChild(ambientLightNode);
        
        // Set up the directional lights
        Vector3f light1Direction  = new Vector3f(60,30,15);

        DirectionalLight light1 = 
                new DirectionalLight(white, light1Direction);
        light1.setInfluencingBounds(bounds);
        scene.addChild(light1);
    }
    
    
    /**
     * Handles anything that have to be placed in our
     * simple universe
     */
    private void buildScene() {
    	tg = new TransformGroup();
    	tg.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
    	
    	// create area for this file
    	tg.addChild( new Area(obj) );
    	
    	scene.addChild(tg);
    }
    
    /**
     * Setup our view so we are able to see the elements
     * in the scene
     */
    private void setupView() {
        ViewingPlatform vp = universe.getViewingPlatform();
        TransformGroup steerTG = vp.getViewPlatformTransform();

        Transform3D t3d = new Transform3D();
        steerTG.getTransform(t3d);

        // args are: viewer posn, where looking, up direction
        t3d.lookAt(new Point3d(60,30,15), 
        		   new Point3d(0,30,0),
        		   new Vector3d(0,0,1)
        	);
        t3d.invert();

        steerTG.setTransform(t3d);
    }

    /**
     * Allos User to move and rotate around the viewpoint
     * @param c Canvas3D object
     */
    private void orbitControls(Canvas3D c) {
    	OrbitBehavior orbit = 
    		new OrbitBehavior(c, OrbitBehavior.REVERSE_ALL);
    	orbit.setSchedulingBounds(bounds);

    	ViewingPlatform vp = universe.getViewingPlatform();
    	vp.setViewPlatformBehavior(orbit);	 
    }
    
    public void mouseClicked(MouseEvent e) {
    	pickCanvas.setShapeLocation(e);
    	PickResult result = pickCanvas.pickClosest();
        if (result == null) {
        	System.out.println("Nothing picked");
        } else {
        	Primitive p = (Primitive)result.getNode(PickResult.PRIMITIVE);
        	Shape3D s = (Shape3D)result.getNode(PickResult.SHAPE3D);
        	
        	//*******************************************************************************
        	//   This code will get you a reference to the sphere clicked on
        	//   notice that it now prints out which sphere you clicked on
        	SceneGraphPath myPath = result.getSceneGraphPath();
        	
        	//System.out.println( myPath.nodeCount() );
    		//System.out.println(myPath.getObject().getClass());
        	
        	if(myPath.getObject() instanceof Shape3D)
        	{
        	//	System.out.println(((Shape3D)myPath.getObject()).getName());
        		//System.out.println(((BranchGroup)myPath.getNode(0)).getName());
        	}
        	//********************************************************************************   
    /**/
        }
    }

	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}
