package starlight.taliis.apps.editors;


import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;

import starlight.alien.*;
import starlight.taliis.core.ZeroTerminatedString;
import starlight.taliis.core.files.dbc;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.util.Vector;
import java.lang.*;

public class dbcRichTable extends tallisStdEditor {
    private dbc cUplink;
    JToolBar toolBar;
    JTable table;
    
    public dbcRichTable(dbc daddy) {
    	cUplink = daddy;

    	this.setLayout(new BorderLayout());
		
		toolBar = new JToolBar();
		JButton button = makeNavigationButton("page_copy", ICON_COPY,
                "Copy selected Row",
                "Copy Row");
		toolBar.add(button);
		
		add(toolBar, BorderLayout.PAGE_START);

    	table = new JTable(new dbcRichModel(cUplink));
        table.setPreferredScrollableViewportSize(new Dimension(100, 70));

        //Create the scroll pane and add the table to it.
        JScrollPane scrollPane = new JScrollPane(table);
        scrollPane.addComponentListener(new
                CorrectStrangeBehaviourListener(table, scrollPane)); 

        //Add the scroll pane to this panel.
        add(scrollPane);
        update();
    }
    
    public void actionPerformed(ActionEvent e) {
        String cmd = e.getActionCommand();
        
        if (ICON_COPY.equals(cmd)) {
        	if(table.getSelectedRow()==-1) return;
        	
        	cUplink.copyRow(table.getSelectedRow());
        	table.updateUI();
        }
    }
    
    /**
     * Data got updated!
     */
    public void update() {    	
    	for(int c=0; c<cUplink.getNFields(); c++) {
    		TableColumn column = table.getColumnModel().getColumn(c);
    		
    		// string?
    		if(cUplink.getColType(c)==dbc.COL_TYPE_STRING) {
    			column.setMinWidth(200);
    			column.setCellEditor(new TableZTSEditor());
    			column.setCellRenderer(new TableColorRenderer(new Color(224,224,255)));
    		}
    		else if(cUplink.getColType(c)==dbc.COL_TYPE_FLOAT) {
    			column.setMinWidth(80);
    			column.setCellRenderer(new TableColorRenderer(new Color(224,255,224)));
    		}
    		else {
    			column.setMinWidth(30);
    			column.setPreferredWidth(60);
    		}    		
    	}
    }
}



class dbcRichModel extends AbstractTableModel {
	dbc archive;
	
	// Bezeichnungen
    Vector columnNames = new Vector();
    
    dbcRichModel(dbc reference) {
    	archive = reference;
    	
    	//TODO: bezeichnungen laden
    	for(int c=0; c<archive.getNFields(); c++) {
    		columnNames.add(c, "#" + c); 
    	}
    }
    
    public int getColumnCount() {
        return (int)archive.getNFields();
    }

    public int getRowCount() {
    	return (int)archive.getNRecords();
    }

    public String getColumnName(int col) {
        return (String) columnNames.get(col);
    }

    public Object getValueAt(int row, int col) {
    	Object t = archive.getData(col, row);
    	int tv = ((Number)t).intValue();
    	
    	int type = archive.getColType(col);

    	if(type==dbc.COL_TYPE_STRING) {
    		ZeroTerminatedString tmp = archive.getStringByOffset(tv);
    		if(tmp==null) System.err.println("ERROR! String " + tv + " not found! " + col);
    		return tmp;
    	}
    	else if(type==dbc.COL_TYPE_FLOAT) {
    		//TODO: this makes the values HORROBLE rounded! find a better way!
    		return Float.intBitsToFloat(tv); // Double.longBitsToDouble(t);
    	}
    	else/**/ return t;
    }

    public Class getColumnClass(int c) {
    	int type = archive.getColType(c);
    	
    	if(type==dbc.COL_TYPE_STRING) return ZeroTerminatedString.class;
    	else if(type==dbc.COL_TYPE_FLOAT) return float.class;
    	else return Integer.class;
    }/**/

    public boolean isCellEditable(int row, int col) {
    	//if(col==0) return false;
    	int type = archive.getColType(col);
    	
    	if(type==dbc.COL_TYPE_STRING) return true;
    	else if(type==dbc.COL_TYPE_FLOAT) return false;
    	else return true;
    }

    public void setValueAt(Object value, int row, int col) {
    	int type = archive.getColType(col);
    	
    	if(type==dbc.COL_TYPE_STRING) {
    		int index = Integer.valueOf((String)value) ;
    		if(archive.getStringByOffset(index)!=null)
    			archive.setData(col, row, index);
    		else System.err.println("Invalid String index.");/**/
    	}
    	else if(type==dbc.COL_TYPE_FLOAT) {
    		//TODO: float eintragen
    	}
    	else {
    		archive.setData(col, row, ((Integer)value).intValue() );
    	}
    	fireTableCellUpdated(row, col);/**/
    }
}