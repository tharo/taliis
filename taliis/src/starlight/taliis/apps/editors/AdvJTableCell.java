package starlight.taliis.apps.editors;

public class AdvJTableCell {
	public Object value;
	public String Tooltip;
	
	public AdvJTableCell(Object o, String text) {
		value = o;
		Tooltip = text;
	}
	
	public String toString() {
		return value.toString();
	}
}
