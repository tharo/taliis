package starlight.taliis.apps.editors;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;

import de.taliis.editor.fileMananger;

import starlight.alien.*;
import starlight.taliis.core.files.*;
import starlight.taliis.core.chunks.*;
import starlight.taliis.core.chunks.adt.MTEX;
import starlight.taliis.core.chunks.adt.MCLY;
import starlight.taliis.helpers.fileLoader;

/**
 * JTable interface for edit MTEX chunks in ADT files
 * @author tharo
  *updated by
 *@author tigurius
 *
 */

public class adtTextureFileTable extends JPanel 
							implements ActionListener {
	adt obj;
	JTable table;
	JToolBar toolBar;
	JTextField newFile;
	
	final String ADD = "add";
	final String DEL = "del";
	
	public adtTextureFileTable(adt reference) {
		obj = reference;
		this.setLayout(new BorderLayout());
	
		newFile = new JTextField();
		newFile.setText("Files are not checked for existance in mpq! (alpha)");
		
		toolBar = new JToolBar();
		JButton  button = makeNavigationButton("delete", DEL,
                "Delete given texture File",
                "Remove Texture");
		toolBar.add(button);
		button = makeNavigationButton("add", ADD,
                "Add a new texture File",
                "Add Texture");
		toolBar.addSeparator();
		toolBar.add(button);
		toolBar.add(newFile);
		add(toolBar, BorderLayout.PAGE_START);
		
        table = new JTable(new mtexTableModel(obj.mtex, obj));
        //table.setPreferredScrollableViewportSize(new Dimension(70, 70));

        //Create the scroll pane and add the table to it.
        JScrollPane scrollPane = new JScrollPane(table);
        scrollPane.addComponentListener(new
                CorrectStrangeBehaviourListener(table, scrollPane)); 

        table.getColumnModel().getColumn(0).setMaxWidth(20);
        table.getColumnModel().getColumn(1).setMaxWidth(30);
        table.getColumnModel().getColumn(3).setMaxWidth(70);

        //Add the scroll pane to this panel.
        add(scrollPane, "Center");
	}
	
	protected JButton makeNavigationButton(String imageName,
            String actionCommand,
            String toolTipText,
            String altText) {
		//Look for the image.
		String imgLocation = "images/icons/"
		+ imageName
		+ ".png";

		//Create and initialize the button.
		JButton button = new JButton();
		button.setActionCommand(actionCommand);
		button.setToolTipText(toolTipText);
		button.addActionListener(this);
		
		if (imageName != "" && imageName != null) {    //image found
			button.setIcon(fileLoader.createImageIcon(imgLocation));
		}
		else {                                     //no image found
			button.setText(altText);
			System.err.println("Resource not found: " + imgLocation);
		}

		return button;
	}

	
    public void actionPerformed(ActionEvent e) {
        String cmd = e.getActionCommand();
        
        if (ADD.equals(cmd)) {
        	obj.mtex.addString(newFile.getText());
        	table.updateUI();
        }
        else if (DEL.equals(cmd)) {
        	JOptionPane.showMessageDialog(this.getParent(),
        			"This function got deactivated.\nRename existing files instead.", 
        		    "He dude ..",
        		    JOptionPane.INFORMATION_MESSAGE);
        }
    }
}



class mtexTableModel extends AbstractTableModel {
	static ImageIcon page = fileLoader.createImageIcon("images/icons/page.png");
	MTEX archive;
	MCLY garchive;
	int [] effectids;
	adt obj;

	
	// Bezeichnungen
    Vector columnNames = new Vector();
    
    mtexTableModel(MTEX reference, adt aspect) {
    	archive = reference;
    	obj	=	aspect;
    	effectids = new int[(int)archive.getLenght()];
    	for(int i=0; i<256;i++){
    		for(int j=0;j<archive.getLenght();j++){
    			for(int n=0;n<obj.mcnk[i].mcly.getLenght();n++){
    				if(obj.mcnk[i].mcly.GetTexID(n)==j) effectids[j]=obj.mcnk[i].mcly.GetGroundEffect(n);	
    			}
    		}
    	}

    }
    

    

    
    public int getColumnCount() {
        return 4;
    }

    public int getRowCount() {
    	return (int)archive.getLenght();
    }

    public String getColumnName(int col) {
        if(col==1) return "ID";
        else if(col==2) return "Filename";
        else if (col==3) return "GeID";
        else return "";
    }

    public Object getValueAt(int row, int col) {
    	if(col==0) return page;
    	else if(col==1) return row;
    	else if(col==2) return archive.getValueNo(row);
    	else if(col==3) return effectids[row];
    	else return null;
    }

    public Class getColumnClass(int c) {
        if(c==0) return Icon.class;
        else return String.class;
    }
    
    public boolean isCellEditable(int row, int col) {
    	if(col==2) return true;
    	else if(col==3) return true;
    	else return false;
    }

    public void setValueAt(Object value, int row, int col) {
    	if(col==2) archive.entrys[row].setString((String)value);
    	if(col==3){
    		int t = 0;
    		try {
    		t = Integer.parseInt ((String)value);
    		} catch (Exception E){
    		//es war keine Zahl
    		}
    		for(int i=0;i<256;i++){
    			obj.mcnk[i].mcly.SetGroundEffect(t, row);
    				for(int n=0;n<obj.mcnk[i].mcly.getLenght();n++){
    					if(obj.mcnk[i].mcly.GetTexID(n)==row){
    						obj.mcnk[i].mcly.SetGroundEffect(t, row);
    						effectids[row]=t;
    					}
    				}
    			}
    		}
    	}

    
    public void clear() {
    }
}