package starlight.taliis.apps.adtedit;

/**
 * This class does nothing else than handeling
 * the given config by first looking for a
 * file in .jars root, then for a default config
 * inside of the archive.
 * 
 * @author tharo
 *
 */
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import starlight.taliis.helpers.fileLoader;
import wowimage.BLPDecoder;
import wowimage.ConversionException;

public class config {
	Properties props;
	
	public config() {
		load("config.txt");
	}
	public config(String fileName) {
		load(fileName);
	}
	
	/**
	 * Loads trys to load the config file.
	 * First external from the file system, then the
	 * .jar fies one.
	 * 
	 * @param fileName
	 */
	private void load(String fileName) {
		InputStream s = fileLoader.getRessourceAsStream(fileName);
		
		props = new Properties();
		try {
			props.load(s);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
		
		// blp bild umformen?
		/*try {
			long start = System.currentTimeMillis();
			
			BLPDecoder en = new BLPDecoder("./files/Moon02Glare.blp");
			en.writePNG("test.png");
			
			long end = System.currentTimeMillis();
			System.out.println("Took: " + (end - start) + "ms");
			
		} catch (ConversionException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		/**/
}
