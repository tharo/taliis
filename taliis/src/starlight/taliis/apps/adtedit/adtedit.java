package starlight.taliis.apps.adtedit;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.plaf.basic.BasicTreeUI;
import javax.swing.tree.*;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.net.URI;

import starlight.taliis.apps.editors.*;
import starlight.taliis.core.files.*;
import starlight.taliis.helpers.*;

public class adtedit 
				implements TreeSelectionListener,
						   ActionListener 		{
	static ImageIcon mIconPlus = fileLoader.createImageIcon("images/icons/bullet_toggle_plus.png");
	static ImageIcon mIconMinus = fileLoader.createImageIcon("images/icons/bullet_toggle_minus.png");
	static ImageIcon mIconNew = fileLoader.createImageIcon("images/icons/page_add.png");
	static ImageIcon mIconSave = fileLoader.createImageIcon("images/icons/disk_multiple.png");
	static ImageIcon mIconOpen = fileLoader.createImageIcon("images/icons/folder.png");
	static ImageIcon mIconExit = fileLoader.createImageIcon("images/icons/door_in.png");
	static ImageIcon mIconFrame = fileLoader.createImageIcon("images/icons/map_edit.png");
	static ImageIcon mIconWHelp = fileLoader.createImageIcon("images/icons/world_go.png");
	static ImageIcon mIconAbout = fileLoader.createImageIcon("images/icons/asterisk_yellow.png");
	
	JFrame top;
	JTree tree;
	JSplitPane sp1;
	
	DefaultMutableTreeNode root;
	JMenuBar menuBar;
	JMenu menFile, menHelp, menEdit;
	JMenuItem meniNew, meniOpen, meniSaveAs, meniExit,
				meniHelp, meniAbout;
	String lastOpenPath = "";
	
	
	/**
	 * main routine
	 * @param args
	 */
	public static void main(String[] args) {
		adtedit a  = new adtedit();
		for(int c=0; c<args.length; c++)
			a.open(args[c]);
	}

	
	adtedit() {
		top = new JFrame();
		top.setTitle("Taliis Editor - (5. yuting edition)");
		top.setSize(700, 400);
		top.setLocation(200, 200);
		top.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );/**/
		top.setLayout(new BorderLayout(100, 0));
		
		// inti components
		createTree();
		createMenue();
		
		JScrollPane treeView = new JScrollPane(tree);
		treeView.setMinimumSize(new Dimension(200, 200));
		sp1 = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
				treeView, new JPanel() );
		sp1.setOneTouchExpandable(true);
		sp1.setDividerLocation(200);
		sp1.setAutoscrolls(false);
		
		top.add(sp1, BorderLayout.CENTER);	
		top.add(menuBar, BorderLayout.NORTH);
	
		top.setIconImage(mIconFrame.getImage());
		top.setVisible(true);
	}
	
	
	
	/**
	 * Opens a new file
	 * @param FileName
	 */
	public void open(String FileName) {
		openFile t = new openFile(FileName);
		
		// insert
		t.createNodes(root);
		treeNodesInserted();
	}

	
	
	/**
	 * Swing is not thread save .. thats why the rebuild
	 * have to be done like this.
	 */
	public void treeNodesInserted() {
		javax.swing.SwingUtilities.invokeLater( 
			new Runnable() {
				public void run() {
					tree.updateUI();
					
					// make it look nice
					tree.expandPath(new TreePath(tree.getModel().getRoot()));
					((BasicTreeUI)tree.getUI()).setExpandedIcon(mIconMinus);
					((BasicTreeUI)tree.getUI()).setCollapsedIcon(mIconPlus);
				} 
			}
		);
	}
	
	
	/**
	 * Creates the on top Menue
	 * 
	 */
	private void createMenue() {
		menuBar	= new JMenuBar();
		menFile = new JMenu("File");
		  menFile.setMnemonic(KeyEvent.VK_F);
		meniNew = new JMenuItem("New", mIconNew);
		  meniNew.setMnemonic(KeyEvent.VK_N);
		  meniNew.addActionListener(this);
		  meniNew.setAccelerator(KeyStroke.getKeyStroke(
		        KeyEvent.VK_N , ActionEvent.CTRL_MASK));
		meniOpen = new JMenuItem("Open", mIconOpen); 
		  meniOpen.setMnemonic(KeyEvent.VK_O);
		  meniOpen.addActionListener(this);
		  meniOpen.setAccelerator(KeyStroke.getKeyStroke(
		        KeyEvent.VK_O , ActionEvent.CTRL_MASK));
		meniSaveAs = new JMenuItem("Save As ..", mIconSave);
		  meniSaveAs.setMnemonic(KeyEvent.VK_A);
		  meniSaveAs.addActionListener(this);
		meniExit = new JMenuItem("Exit", mIconExit);
		  meniExit.setAccelerator(KeyStroke.getKeyStroke(
		        KeyEvent.VK_Q, ActionEvent.CTRL_MASK));
		  meniExit.setMnemonic(KeyEvent.VK_X);
		  meniExit.addActionListener(this);
		  
		menFile.add(meniNew);
		menFile.addSeparator();
		menFile.add(meniOpen);
		menFile.add(meniSaveAs);
		menFile.addSeparator();
		menFile.add(meniExit);
		menuBar.add(menFile);
		
		/*menEdit = new JMenu("Edit");
		menEdit.setEnabled(false);
		menuBar.add(menEdit);
		/**/
		menHelp = new JMenu("Help");
		  menFile.setMnemonic(KeyEvent.VK_H);
		meniHelp = new JMenuItem("Webpage", mIconWHelp);
		  meniHelp.addActionListener(this);
		meniAbout = new JMenuItem("Info", mIconAbout);
		  meniAbout.addActionListener(this);
		
		menHelp.add(meniHelp);
		menHelp.add(meniAbout);
		menuBar.add(menHelp);
	}
	
	
	
	/**
	 * Creates the navigation tree on the left side
	 */
	private void createTree() {
		root = new DefaultMutableTreeNode(new MenueEntry("", 666, null));
		
		tree = new JTree(root);
		tree.putClientProperty("JTree.lineStyle", "Horizontal");
		tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
		tree.setCellRenderer(new MenueRenderer());
		tree.setRootVisible(false);
		tree.setShowsRootHandles(false);
		//Listen for when the selection changes.
		tree.addTreeSelectionListener(this);
	}

	
	
	
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==meniExit)
			System.exit(0);
		else if(e.getSource()==meniNew) {
			newADTDialog n = new newADTDialog(top.getComponent(0));
			if(n.ok==true) { 
				openFile t = new openFile(new adt(), 
						n.name + "_" + n.xStr + "_" + n.yStr + ".adt"
					);
				
				adtCoordHelper chelp = new adtCoordHelper((adt)t.obj);
				chelp.calcCoordinates(n.y, n.x, 0, 0);
				
				// insert
				t.createNodes(root);
				treeNodesInserted();
			}
		}
		// open
		else if(e.getSource()==meniOpen) {
			final JFileChooser fc = new JFileChooser();
			fc.setCurrentDirectory( new File(lastOpenPath));
			
			fc.showOpenDialog(top.getComponent(0));
			
			if(fc.getSelectedFile()==null) return;
			lastOpenPath = fc.getSelectedFile().getPath();
			
			String fn = fc.getSelectedFile().getName();
			if(fn.endsWith(".adt")) {
				this.open(fc.getSelectedFile().getAbsolutePath());
			}
			else if(fn.endsWith(".dbc")) {
				this.open(fc.getSelectedFile().getAbsolutePath());
			}
		}
		// save
		else if(e.getSource()==meniSaveAs) {
			// file selected?
			DefaultMutableTreeNode node = (DefaultMutableTreeNode)
			tree.getLastSelectedPathComponent();

			if (node == null) {
				System.err.println("No file selected");
				return;
			}
			Object nodeInfo = node.getUserObject();
			MenueEntry entr = (MenueEntry)nodeInfo;
		
			// file saver anzeigen
			JFileChooser fc = new JFileChooser();
			fc.setCurrentDirectory( new File(lastOpenPath));
			fc.showSaveDialog(top.getComponent(0));
			
			// file speichern
			if(fc.getSelectedFile()==null) return;
			lastOpenPath = fc.getSelectedFile().getPath();
			
			// adt files
			if(entr.myFile.obj.getClass()==adt.class) {
		    	adt o = (adt)entr.myFile.obj;
		    	
		    	o.render();
				// render appereances
				adtObjHelper objh = new adtObjHelper( o );
				objh.generateAppeareances();
				
				adtChecker check = new adtChecker( o );
				if(check.check()==0)	
					fileLoader.saveBuffer(o.buff, fc.getSelectedFile().getAbsolutePath());
		    	
			}
			// dbc files
			else if(entr.myFile.obj.getClass()==dbc.class) {
				dbc o = (dbc)entr.myFile.obj;
				o.render();
				
				fileLoader.saveBuffer(o.buff, fc.getSelectedFile().getAbsolutePath());
			}
			else System.err.println("Error: Save is not supported for this file type.");
		}
		else if(e.getSource()==meniHelp) {
			Desktop desk = Desktop.getDesktop();
			if(desk!=null) {
				try {
					desk.browse(new URI("http://taliis.blogspot.com"));
				} catch(Exception ex) {	}
			}		
		}
		else if(e.getSource()==meniAbout) {
			new infoDialog(this.top);
		}
	}
	
	/**
	 * Something in the tree got selected
	 */
	public void valueChanged(TreeSelectionEvent e) {
		DefaultMutableTreeNode node = (DefaultMutableTreeNode)
	                       tree.getLastSelectedPathComponent();
	
	    if (node == null) return;

	    Object nodeInfo = node.getUserObject();
	    MenueEntry entr = (MenueEntry)nodeInfo;
	    
	    if(entr.myFile.obj.getClass()==adt.class) {
	    	adt o = (adt)entr.myFile.obj;
	    	switch(entr.id) {
	        	case 11: sp1.setRightComponent(new adtTextureFileTable(o)); break;
	        	case 12: sp1.setRightComponent(new adtDoodadFileTable(o)); break;
	        	case 13: sp1.setRightComponent(new adtObjectFileTable(o)); break;
	        	case 21: sp1.setRightComponent(new adtDoodadAppTable(o)); break;
	        	case 22: sp1.setRightComponent(new adtObjectAppTable(o)); break;
	        	case 3: sp1.setRightComponent(new adtJ3DVisual(o)); break;
	        }
	    }
	    else if(entr.myFile.obj.getClass()==dbc.class) {
	    	dbc o = (dbc)entr.myFile.obj;
	    	switch(entr.id) {
	    		case 1: sp1.setRightComponent(new dbcRichTable(o)); break;
	    		case 2: sp1.setRightComponent(new dbcSimpleTable(o)); break;
	    		case 3: sp1.setRightComponent(new dbcStringTable(o)); break;
	    	}
	    }
	}
}

class MenueRenderer extends DefaultTreeCellRenderer {
	public Component getTreeCellRendererComponent(
                        JTree tree,
                        Object value,
                        boolean sel,
                        boolean expanded,
                        boolean leaf,
                        int row,
                        boolean hasFocus) {

        super.getTreeCellRendererComponent(
                        tree, value, sel,
                        expanded, leaf, row,
                        hasFocus);
        MenueEntry nodeInfo = (MenueEntry)((DefaultMutableTreeNode) value).getUserObject();
	    if (nodeInfo.ico!=null) {
	    	setIcon(nodeInfo.ico);
	    } //setToolTipText("This book is in the Tutorial series.");
        /**/
	    return this;
    }
}
