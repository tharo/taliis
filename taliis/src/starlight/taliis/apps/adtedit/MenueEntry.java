package starlight.taliis.apps.adtedit;

import javax.swing.Icon;

import starlight.taliis.core.chunks.chunk;

class MenueEntry {
	String name;
	chunk dataChunk;
	public openFile myFile;
	public int id;
	public Icon ico = null;
	
	MenueEntry(String Name, int ID, openFile ref) {
		name = Name;
		id = ID;
		myFile = ref;
	}
	MenueEntry(String Name, int ID, openFile ref, Icon i) {
		name = Name;
		id = ID;
		myFile = ref;
		ico = i;
	}
	MenueEntry(String Name, int ID, openFile ref, Icon i, chunk cRef) {
		name = Name;
		id = ID;
		myFile = ref;
		ico = i;
		dataChunk = cRef;
	}
	
	public String toString() {
		if(dataChunk==null) return name;
		
		if(dataChunk.getLenght()!=-1) {
			return name + " (" + dataChunk.getLenght() + ")";
		}
		else return name;
	}
}