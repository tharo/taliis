package starlight.taliis.helpers;

/**
 * This class provides functions that allows to 
 * create / modify / delete DD and WMO Objects inside of a map.
 * It do NOT handle the registration/management of the Filenames itself.
 * 
 * @author tharo
 *
 */

import starlight.taliis.core.files.*;
import starlight.taliis.core.chunks.*;
import starlight.taliis.core.chunks.wdt.MODF_Entry;

public class wdtObjHelper {
	wdt obj;
	
	public wdtObjHelper(wdt mainObject) {
		obj = mainObject;
	}

	
	public int addWMO(int WMOFileID, int globalID, int x, int y, int z) {
		// create appeareance
		int modfid = obj.modf.addObject(WMOFileID, globalID,x, y, z 
			);
		
		// calculate fake ext values
		obj.modf.entrys[modfid].calcExtValues();
		
		return modfid;
	}
	
	/**
	 * Adds a new WMO or such-called "object" at the given
	 * position  and register it in the MCNK chunk.
	 * 
	 * @param WMOFileID	ID were the wmo file got loaded with
	 * @param globalID	Global and unique ID in this instance
	 * @param x			x coordinates (absolute)
	 * @param y			y coordinates (absolute)
	 * @param z			y coordinates (absolute)
	 * @return			index of the entry in MODF list
	 */
	public int addWMO(int WMOFileID, int globalID,
			float x, float y, float z) {
		
		// create appeareance
		int modfid = obj.modf.addObject(WMOFileID, globalID, 
				MODF_Entry.translate(x), 
				MODF_Entry.translate(y), 
				z
			);
		
		// calculate fake ext values
		obj.modf.entrys[modfid].calcExtValues();

		
		
		
		return modfid;
	}
}
	
		

	
