package starlight.taliis.core.binary.skin;

import java.nio.ByteBuffer;

import starlight.taliis.core.memory;

public  class TexUnits extends memory{
	public static int Flags = 0x0;
	public static int RenderOrder = 0x2;
	public static int SubmeshIndex = 0x4;
	public static int SubmeshIndex2 =0x6;
	public static int ColorIndex =0x8;
	public static int RenderFlags=0xA;
	public static int TexUnitNumber=0xC;
	public static int Unknown=0xE;
	public static int Texture=0x10;
	public static int TexUnitNumber2=0x12;
	public static int Transparency=0x14;
	public static int TextureAnim=0x16;
	public static int end=0x18;
	
	public TexUnits(ByteBuffer databuffer) {
		//TODO: push pointer by our own size
		super(databuffer);
		
		buff.limit( end );
		databuffer.position( databuffer.position() + buff.limit());
	}

	public TexUnits() {
		// TODO Auto-generated constructor stub
		super(end);
		buff.putShort(Flags,(short)16);
		buff.putShort(ColorIndex,(short) -1);
		buff.putShort(Unknown,(short) 1);
		buff.position(0);
	}
	
	public void render(){
		buff.position(0);
	}
	public short getSubmeshIndex(){
		return buff.getShort(SubmeshIndex);
	}
	public void setSubmeshIndex(short val){
		buff.putShort(SubmeshIndex, val);
		buff.putShort(SubmeshIndex2, val);
	}
	public short getTexture(){
		return buff.getShort(Texture);
	}
	public void setTexture(short val){
		buff.putShort(Texture, val);
	}
	public short getTransparency(){
		return buff.getShort(Transparency);
	}
	public void setTransparency(short val){
		buff.putShort(Transparency, val);
	}
}