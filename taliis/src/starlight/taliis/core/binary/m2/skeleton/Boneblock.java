package starlight.taliis.core.binary.m2.skeleton;

/**
 * This block defines a Bone
 * 
 * @author tigurius
 */

import java.nio.*;

import starlight.taliis.core.memory;
import starlight.taliis.core.binary.m2.AnimationBlock;

public class Boneblock extends memory{
	public static int AnimationSeq = 0x0;//Index into Animation sequences or -1. 
	public static int Flag = 0x4;//Only known flags: 8 - billboarded and 512 - transformed 
	public static int ParentBone = 0x8;
	public static int GeosetID = 0xC;
	public static int Unknown = 0x10;//4 bytes that have been in since TBC. Noone knows what they are for.
	public static int Translation=0x14;//AnimBlock
	public static int Rotation=0x28;//AnimBlock
	public static int Scaling=0x3C;//AnimBlock
	public static int PivotPoint=0x50;//3*float
	public static int end=0x5C;
	
	AnimationBlock TranslationBlock;
	AnimationBlock RotationBlock;
	AnimationBlock ScalingBlock;
	
	public Boneblock(ByteBuffer databuffer){
		super(databuffer);

		buff.limit( end );
		
		databuffer.position( databuffer.position() + buff.limit());
	}
	
	/**
	 * Returns the AnimationSequence
	 * 
	 * @return int
	 */
	public int getAnimSequence(){
		return buff.getInt(AnimationSeq);
	}
	
	/**
	 * Sets the AnimationSequence
	 * 
	 * @param int
	 */
	public void setAnimSequence(int val){
		buff.putInt(AnimationSeq, val);
	}
}