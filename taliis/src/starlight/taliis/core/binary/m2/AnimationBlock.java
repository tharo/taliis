package starlight.taliis.core.binary.m2;

/**
 * A simple Animation-Block :O
 * 
 * points to an animsubstructure
 * 
 * 
 * @author Tigurius
 */

import java.nio.*;

import starlight.taliis.core.memory;

public  class AnimationBlock extends memory{
	public static short InterpolationType=0x00;
	public static short GlobalSequenceID=0x2;
	public static int nTimeSubstructures=0x4;
	public static int ofsTimeSubstructures=0x8;
	public static int nKeySubstructures=0xC;
	public static int ofsKeySubstructures=0x10;
	public static int end=0x14;
	
	public AnimationBlock(ByteBuffer databuffer){
		super(databuffer);
		
		buff.limit( end );
		databuffer.position( databuffer.position() + buff.limit());
	}
	public AnimationBlock(){
		super(end);
		buff.putShort(GlobalSequenceID,(short) -1);
		buff.position(0);
	}
	
	public int getnTimeSubstructures(){
		return buff.getInt(nTimeSubstructures);
	}
	public int getofsTimeSubstructures(){
		return buff.getInt(ofsTimeSubstructures);
	}
	public void setnTimeSubstructures(int val){
		buff.putInt(nTimeSubstructures,val);
	}
	public void setofsTimeSubstructures(int val){
		buff.putInt(ofsTimeSubstructures,val);
	}
	public int getnKeySubstructures(){
		return buff.getInt(nKeySubstructures);
	}
	public int getofsKeySubstructures(){
		return buff.getInt(ofsKeySubstructures);
	}
	public void setnKeySubstructures(int val){
		buff.putInt(nKeySubstructures,val);
	}
	public void setofsKeySubstructures(int val){
		buff.putInt(ofsKeySubstructures,val);
	}
}