package starlight.taliis.core.files;

import java.io.InvalidClassException;
import java.nio.ByteBuffer;

import starlight.taliis.core.chunks.*;

/**
 * This is the wmo file class.
 * 
 * Since wmo files are splitted into
 * wmo_root and wmo_group files this class
 * is for static usage ONLY!
 * 
 * It can be used to load an unknown wmo file
 * into the correspodending sub classes.
 * 
 * Also and it can be used for the global render process.
 * 
 * @author ganku
 *
 */

public class wmo extends wowfile {
	// different versions
	public static final int WMO_VERSION_ALPHA = 0x0E;
	public static final int WMO_VERSION_NORMAL = 0x11;	// whatever normal means 
	public static final int WMO_VERSION_TALIIS = 0x12;//created by Taliis
	/**
	 * pass the default memory constructor
	 * @param databuffer
	 */
	public wmo(ByteBuffer databuffer) throws InvalidClassException {
		super(databuffer);
	}
	protected wmo(){};
	
	/**
	 * Trys to init the wmo file into its given
	 * class structure and return thoose.
	 * 
	 * It do not handles version checkup since the
	 * sub classes might have different development
	 * states.
	 * 
	 * @param databuffer
	 * @return wmo_root or wmo_group object. or null.
	 * @throws ChunkNotFoundException 
	 */
	public static wmo load(ByteBuffer databuffer) throws InvalidClassException, ChunkNotFoundException {
		// get mver
		try {
			MVER mver = new MVER(databuffer);
		} catch(ChunkNotFoundException e) {
			throw new InvalidClassException(e.getMessage());
		}
			
		if(chunk.nextChunk(databuffer).compareTo("DHOM")==0) {
			// root file
			databuffer.position(0);
			return new wmo_root(databuffer);
		}
		else if(chunk.nextChunk(databuffer).compareTo("PGOM")==0) {
			// group file
			databuffer.position(0);
			return new wmo_group(databuffer);
		}
		else {
			System.err.println("Unknown wmo structure!");
			return null;
		}
	}
	
	/**
	 * Takes care of the rendering of all objects related
	 * to this wmo group. Then take care of the root object.
	 * 
	 * @param rootObject
	 * @param wmoGroups
	 * @return >=0 if rendering successed
	 */
	public static int renderGroup(wmo_root rootObject, wmo_group[] wmoGroups) {
		//first: render all group files
		for(wmo_group g : wmoGroups) {
			g.render();
		}
		
		// now handle the main file rendering
		
		return -1;
	}
	
	
	/**
	 * just here to have it compatible to all
	 * of our subclasses.
	 * @return
	 */
	public int getVersion(){
		return -1;
	}
}
