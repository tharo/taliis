package starlight.taliis.core.files;

/**
 * Blizzards WDL world holding format
 * http://www.madx.dk/wowdev/wiki/index.php?title=WDL
 */

import java.io.InvalidClassException;
import java.nio.*;


import starlight.taliis.core.chunks.*;
import starlight.taliis.core.chunks.wdl.MAOF;
import starlight.taliis.core.chunks.wdl.MARE;
import starlight.taliis.core.chunks.wdl.MAHO;


public class wdl extends wowfile {
	MVER mver;
    public MAOF maof;
    public MARE mare[];
    public MAHO maho[];
    public boolean has_maho=false;
	
	/**
	 * Read in a complete wdl file from the given
	 * data buffer 
	 * @param databuffer
	 * @throws ChunkNotFoundException 
	 */
	public wdl(ByteBuffer databuffer) throws InvalidClassException, ChunkNotFoundException {
		super(databuffer);
		
		try {
			mver = new MVER(buff);
			maof = new MAOF(buff);
		} catch(ChunkNotFoundException e) {
			throw new InvalidClassException(e.getMessage());
		}
		
		mare = new MARE[4096];
		maho = new MAHO[4096];
		for(int x=0;x<64;x++){
			for(int y=0;y<64;y++){
				if(maof.getOffset(x, y)!=0){
					buff.position(maof.getOffset(x, y));
					try {
						mare[y+x*64] = new MARE(buff);
						if(databuffer.hasRemaining()&& chunk.nextChunk(databuffer).compareTo("OHAM")==0)
						{maho[y+x*64] = new MAHO(buff);
						has_maho=true;
						}
					} catch(ChunkNotFoundException e) {
						throw new InvalidClassException(
								e.getMessage() + 
								"(" + x +","+ y +")"
							);
					}
				}
				else{
					mare[y+x*64]=null;
					maho[x*64+y]=null;
				}
			}
		}
	}
	
	/**
	 * creates a complete new and empty world, but has empty
	 * hightmaps for every tile
	 * @throws ChunkNotFoundException 
	 */
	public wdl() throws ChunkNotFoundException {
		mver = new MVER();
		maof = new MAOF();
		mare = new MARE[4096];
		maho = new MAHO[4096];
		/*for(int x=0;x<64;x++){
			for(int y=0;y<64;y++){
				maof.setOffset(x, y, (4096*4+x*y*545*2+x*y*0x20));
				mare[y+x*64] = new MARE();
				
				maho[y+x*64] = new MAHO();
			}
			
		}*/
		
	}
	
	public void render() {
		int ro = 0;	
		
		mver.render();
		ro+=mver.getSize();
		
		maof.render();
		ro+=maof.getSize();
		
		for(int x=0;x<64;x++){
			for(int y=0;y<64;y++){
				if(mare[y+x*64]!=null){
					maof.setOffset(x, y, ro);
					mare[y+x*64].render();
					ro+=mare[y+x*64].getSize();
					if(has_maho==true){
					if(maho[x*64+y]==null){
						maho[x*64+y]=new MAHO();
					}
					maho[y+x*64].render();
					ro+=maho[y+x*64].getSize();
					}
				}
			}
		}
		
		

		
		buff = ByteBuffer.allocate(ro);
		buff.order(ByteOrder.LITTLE_ENDIAN);
		buff.position(0);
		
		buff.put(mver.buff);
		buff.put(maof.buff);
		for(int x=0;x<64;x++){
			for(int y=0;y<64;y++){
				if(mare[y+x*64]!=null){
					
					buff.put(mare[y+x*64].buff);
					if(has_maho==true)
					buff.put(maho[y+x*64].buff);

				}
			}
		}

		buff.position(0);
	}
}
