package starlight.taliis.core.files;

import java.awt.image.BufferedImage;
import java.io.InvalidClassException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import starlight.taliis.core.chunks.ChunkNotFoundException;
import starlight.taliis.core.chunks.MVER;
import starlight.taliis.core.chunks.SubChunkNotFoundException;
import starlight.taliis.core.chunks.unkChunk;
import starlight.taliis.core.chunks.adt.MCAL_Entry;
import starlight.taliis.core.chunks.adt.MCIN;
import starlight.taliis.core.chunks.adt.MCNK;
import starlight.taliis.core.chunks.adt.MDDF;
import starlight.taliis.core.chunks.adt.MFBO;
import starlight.taliis.core.chunks.adt.MHDR;
import starlight.taliis.core.chunks.adt.MMDX;
import starlight.taliis.core.chunks.adt.MMID;
import starlight.taliis.core.chunks.adt.MODF;
import starlight.taliis.core.chunks.adt.MTEX;
import starlight.taliis.core.chunks.adt.MTFX;
import starlight.taliis.core.chunks.adt.MWID;
import starlight.taliis.core.chunks.adt.MWMO;

/**
 * Blizzard adt map file http://wowdev.org/wiki/index.php/ADT
 * 
 * UNDER CONSTRUCTION !!! Due some changes in the ADT file that had not caused a
 * new MVER Chunk AND because of motherfucking noggit errors, I decided that the
 * loading function will leave the "structural" order and go to an offset based
 * parsing.
 * 
 * MCNK will have the same changes later.
 * 
 * @author tharo
 * 
 */
@SuppressWarnings("unused")
public final class adt extends wowfile {
	// flags for format
	public static final int ADT_VERSION_NORMAL = 0;
	public static final int ADT_VERSION_EXPANSION_1 = 1;
	public static final int ADT_VERSION_EXPANSION_TBC = 1;
	public static final int ADT_VERSION_EXPANSION_2 = 2;
	public static final int ADT_VERSION_EXPANSION_WOTLK = 2;

	int version_flag = ADT_VERSION_NORMAL;
	

	// chunks
	public MVER info;
	public MHDR header;
	public MCIN fieldInfo;

	public MTEX mtex;

	public MMDX mmdx;
	public MMID mmid;

	public MWMO mwmo;
	public MWID mwid;

	public MDDF mddf;
	public MODF modf;
	unkChunk mh2o;
	public MCNK mcnk[];
	
	//optional
	public MFBO mfbo = null;
	public MTFX mtfx=null;
	

	/**
	 * constructor for opening files
	 * 
	 * @param databuffer ByteBuffer of the data stream
	 * @throws InvalidClassException 
	 * @throws InvalidClassException If its not an ADT file.
	 */
	public adt(ByteBuffer databuffer) throws InvalidClassException {
		super(databuffer);
		boolean tmp;
		try {
			load(databuffer);
			return;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			try {
				System.out.println("Offsets are not valid!");
				parse(databuffer);
			} catch (InvalidClassException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (ChunkNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		


	}

	/**
	 * constructor for creating a NEW adt file ADT expect then that a wow client
	 * 1.0-2.4 format is used
	 */
	public adt() {
		init();
	}

	/**
	 * constructor for creating adt for a specific version
	 * 
	 * @param version Which client version. See adt.ADT_VERSION_*.
	 */
	public adt(int version) {
		version_flag = version;
		init();
	}

	private void init() {
		// create all chunks
		info = new MVER();

		header = new MHDR();
		fieldInfo = new MCIN();

		mtex = new MTEX();

		mmdx = new MMDX();
		mmid = new MMID();

		mwmo = new MWMO();
		mwid = new MWID();

		mddf = new MDDF();
		modf = new MODF();

		// if(version_flag==ADT_VERSION_EXPANSION_WOTLK)
		// mh2o = new MH2O();

		mcnk = new MCNK[256];
		for (int c = 0; c < 16; c++)
			for (int i = 0; i < 16; i++)
				mcnk[(c * 16) + i] = new MCNK(i, c, version_flag);

		// naah i dont trust this atm ..
		/*
		 * if(version_flag>=ADT_VERSION_EXPANSION_TBC) { mh2o = new MH2O(); }
		 */
	}

	/**
	 * Outsourced reading sequence
	 * 
	 * @throws ChunkNotFoundException
	 */
	private boolean load(ByteBuffer databuffer) throws InvalidClassException {
		// read the version and the header
		try {
			info = new MVER(buff);
		} catch (ChunkNotFoundException e) {
			throw new InvalidClassException("No ADT Version found.");
		}
		
		try {
			header = new MHDR(buff);
		} catch (ChunkNotFoundException e) {
			throw new InvalidClassException("No ADT Headers found.");
		}

		checkVersion();

		// load stuff using the offsets
		int base_offs = header.getBaseOffs();

		try {
			buff.position(base_offs + header.getInfoOffs());
			fieldInfo = new MCIN(buff);

			buff.position(base_offs + header.getTexOffs());
			mtex = new MTEX(buff);

			buff.position(base_offs + header.getModelOffs());
			mmdx = new MMDX(buff);
			buff.position(base_offs + header.getModelIDOffs());
			mmid = new MMID(buff);

			buff.position(base_offs + header.getMapObjOffs());
			mwmo = new MWMO(buff);
			buff.position(base_offs + header.getMapObjIDOffs());
			mwid = new MWID(buff);

			buff.position(base_offs + header.getDooDsDefOffs());
			mddf = new MDDF(buff);
			buff.position(base_offs + header.getObjDefOffs());
			modf = new MODF(buff);

			if (version_flag >= ADT_VERSION_EXPANSION_TBC)
				if (header.getoffsMH2O() != 0) {
					buff.position(base_offs + header.getoffsMH2O());
					mh2o = new unkChunk(buff, "O2HM");

				}
		} catch (ChunkNotFoundException e) {
			// TODO: try to find it manually
			throw new InvalidClassException(e.getMessage());
		}

		try {
			mcnk = new MCNK[256];
			for (int c = 0; c < 256; c++) {
				buff.position(fieldInfo.getOffs(c));
				mcnk[c] = new MCNK(buff, version_flag);
			}
		} catch (SubChunkNotFoundException e) {
			throw new InvalidClassException(e.getMessage());
		} catch (ChunkNotFoundException e) {
			// TODO: try to find it manually
			throw new InvalidClassException(e.getMessage());
		}

		if (version_flag >= ADT_VERSION_EXPANSION_TBC)
			try {
				if (header.getoffsFlightBoundary() != 0) {
					buff.position(base_offs + header.getoffsFlightBoundary());
					mfbo = new MFBO(buff);
				}
			} catch (ChunkNotFoundException e) {
				// TODO: try to find it manually
				throw new InvalidClassException(e.getMessage());
			}
			
		if (version_flag >= ADT_VERSION_EXPANSION_WOTLK){
			try{
				if (header.getoffsMTFX() != 0) {
					buff.position(base_offs + header.getoffsMTFX());
					mtfx = new MTFX(buff);
				}
			}
			catch(ChunkNotFoundException e){
				// TODO: try to find it manually
				throw new InvalidClassException(e.getMessage());
			}
		}

		// clean return
		buff.limit(buff.position());
		databuffer.position(databuffer.position() + buff.limit());
		return true;
	}
	
	private void parse(ByteBuffer databuffer) throws InvalidClassException, ChunkNotFoundException{
		// read the version and the header
		buff.position(0);
		try {
			info = new MVER(buff);
		} catch (ChunkNotFoundException e) {
			throw new InvalidClassException("No ADT Version found.");
		}
		
		try {
			header = new MHDR(buff);
		} catch (ChunkNotFoundException e) {
			throw new InvalidClassException("No ADT Headers found.");
		}

		checkVersion();

			fieldInfo = new MCIN(buff);

			mtex = new MTEX(buff);

			mmdx = new MMDX(buff);
			
			mmid = new MMID(buff);

			mwmo = new MWMO(buff);
			
			mwid = new MWID(buff);

			
			mddf = new MDDF(buff);
			
			modf = new MODF(buff);

			if (version_flag >= ADT_VERSION_EXPANSION_TBC)
				if (header.getoffsMH2O() != 0) {
					
					mh2o = new unkChunk(buff, "O2HM");
				}



			mcnk = new MCNK[256];
			for (int c = 0; c < 256; c++) {
				mcnk[c] = new MCNK(buff, version_flag);
			}


		if (version_flag >= ADT_VERSION_EXPANSION_TBC)
				if (header.getoffsFlightBoundary() != 0) {
					
					mfbo = new MFBO(buff);
				}


		// clean return
		buff.limit(buff.position());
		databuffer.position(databuffer.position() + buff.limit());
	}

	/**
	 * Well. Our very own nasty way to check the version of an adt file.
	 * 
	 * Have to be called AFTER MVER und Header got parsed!
	 * 
	 */
	private void checkVersion() {
		if (header == null || info == null)
			return;

		// so far the mver number we know
		if (info.getVersion() == 0x12) {
			// detect needed version
			// !!!!! ALWAYS double check later because this checkup
			// may change and the TBC is NO sure reason that a
			// flight boundary is set !!
			if (header.getoffsFlightBoundary() != 0)
				version_flag = ADT_VERSION_EXPANSION_TBC;
			if (header.getoffsMH2O() != 0)
				version_flag = ADT_VERSION_EXPANSION_WOTLK;
			if (header.getoffsMTFX() != 0)
				version_flag = ADT_VERSION_EXPANSION_WOTLK;
		}
	}

	/**
	 * Renders all data hold in classes into buffer
	 */
	@Override
	public void render() {
		// calculate offsets
		int ro = 0; // running offset

		// transform to local offset (beginning at MHDR.data)
		info.render();
		// ro+=info.getSize();

		ro -= 0x8;
		header.render();
		ro += header.getSize();

		header.setInfoOffs(ro);
		fieldInfo.render();
		ro += fieldInfo.getSize();

		header.setTexOffs(ro);
		mtex.render();
		ro += mtex.getSize();

		header.setModelOffs(ro);
		mmdx.render();
		ro += mmdx.getSize();

		header.setModelIDOffs(ro);
		mmid.render(mmdx);
		ro += mmid.getSize();

		header.setMapObjOffs(ro);
		mwmo.render();
		ro += mwmo.getSize();

		header.setMapObjIDOffs(ro);
		mwid.render();
		mwid.render(mwmo);
		ro += mwid.getSize();

		header.setDooDsDefOffs(ro);
		mddf.render();
		ro += mddf.getSize();

		header.setObjDefOffs(ro);
		modf.render();
		ro += modf.getSize();

		if (mh2o != null) {
			header.setoffsMH2O(ro);
			mh2o.render();
			ro += mh2o.getSize();
		}

		// transform back to global offset
		ro += info.getSize() + 0x8;
		for (int c = 0; c < 256; c++) {
			// render fields and register them back into info chunk
			mcnk[c].render();
			fieldInfo.setOffs(c, ro, mcnk[c].getSize());

			ro += mcnk[c].getRenderedSize();
		}

		if (mfbo != null) {
			header.setoffsFlightBoundary(ro);
			mfbo.render();
			ro += mfbo.getSize();
		}

		// malloc new memory
		buff = ByteBuffer.allocate(ro);
		buff.order(ByteOrder.LITTLE_ENDIAN);
		buff.position(0);

		// write them down to new buffer
		buff.put(info.buff);
		buff.put(header.buff);
		buff.put(fieldInfo.buff);
		buff.put(mtex.buff);
		buff.put(mmdx.buff);
		buff.put(mmid.buff);
		buff.put(mwmo.buff);
		buff.put(mwid.buff);
		buff.put(mddf.buff);
		buff.put(modf.buff);
		if (mh2o != null)
			buff.put(mh2o.buff);

		for (int c = 0; c < 256; c++)
			buff.put(mcnk[c].buff); // write chunk itself

		if (mfbo != null)
			buff.put(mfbo.buff);

		buff.position(0);
	}

	/**
	 * @return The ADT file subversion that is used
	 */
	public int getVersion() {
		return version_flag;
	}

	
}
