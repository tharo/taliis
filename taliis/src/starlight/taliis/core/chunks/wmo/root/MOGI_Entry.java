package starlight.taliis.core.chunks.wmo.root;

import java.nio.ByteBuffer;

import starlight.taliis.core.memory;

public class MOGI_Entry extends memory {
	/*000h*/public final static int flags	= 0x0;
	/*004h*/public final static int bb1x	= 0x4;
			public final static int bb1y	= 0x8;
			public final static int bb1z	= 0xC;
	/*010h*/public final static int bb2x	= 0x10;
			public final static int bb2y	= 0x14;
			public final static int bb2z	= 0x18;
	/*01Ch*/public final static int snIndex= 0x1c;
			public final static int end 	= 0x20;
			
			
	public MOGI_Entry(ByteBuffer pointer) {
		super(pointer);
		pointer.position( pointer.position() + end);
	}
	public MOGI_Entry() {
		super(end);
		buff.position(0);
	}
	
	public int getFlag(){
		return buff.getInt(flags);
	}
	public void setFlag(int val){
		buff.putInt(flags, val);
	}
	
	public int getSetNameIndex() {
		return buff.getInt(snIndex);
	}
}
