package starlight.taliis.core.chunks.wmo.root;

import java.nio.ByteBuffer;

import starlight.taliis.core.memory;

public class MOMT_Material extends memory {
	public final static int flags 					= 0x0;//?
	public final static int blending				= 0x8;
	public final static int starttexname			= 0xC;//Start position for the texture filename in the MOTX data block
	public final static int endtexname				= 0x18;//End position for the texture filename in the MOTX data block
	public final static int end						= 0x40;
	
	
	MOMT_Material(ByteBuffer pointer){
		super(pointer);
		
		buff.limit(end);
		pointer.position( pointer.position() + buff.limit());
	}
	
	MOMT_Material() {
		super(end);
		buff.putInt(0x10, 0x000000FF);//in the blizz files there is very often 0x0000000ff so put it in there o.O
		// reset position
		buff.position(0);
		
		if(DEBUG) System.out.println("    New MOMT Material created.");
	}
	
	public void render(){
		buff.position(0);
	}
	
	public int getFlags(){
		return buff.getInt(flags);
		
	}
	public void setFlags(int val){
		buff.putInt(flags, val);
	}
	
	public int getBlending(){
		return buff.getInt(blending);
		
	}
	public void setBlending(int val){
		buff.putInt(blending, val);
	}
	
	public void setStartTex(int val) {
		buff.putInt(starttexname, val);
	}
	public int getStartTex() {
		return buff.getInt(starttexname);
	}
	public void setEndTex(int val) {
		buff.putInt(endtexname, val);
	}
	public int getEndTex() {
		return buff.getInt(endtexname);
	}
	
}