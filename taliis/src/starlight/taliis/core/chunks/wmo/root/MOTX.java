package starlight.taliis.core.chunks.wmo.root;

import java.nio.ByteBuffer;

import starlight.taliis.core.ZeroPaddedString;
import starlight.taliis.core.ZeroTerminatedString;
import starlight.taliis.core.chunks.ChunkNotFoundException;
import starlight.taliis.core.chunks.chunk;

/**
 * Zero padded String list that holds the wmo texture 
 * file names.
 * 
 * http://madx.dk/wowdev/wiki/index.php?title=WMO#MOTX_chunk
 * 
 * @author ganku
 *
 */

public class MOTX extends chunk {
	public ZeroPaddedString entrys[] = null;
	public int length=0;
	int size;
	public int index[];
	boolean haschanged;
	
	/**
	 * loads cound strings from the given pointer and
	 * hold it into this chunk.
	 * 
	 * @param pointer
	 * @param count
	 */
	public MOTX(ByteBuffer pointer) throws ChunkNotFoundException {
		super(pointer, "XTOM");

		size = buff.getInt();
		buff.limit(buff.position() + size);
		
		// how many strings?
		int count = 0;
		
		byte chr=0, old_chr=1;
		 while(buff.hasRemaining()) {
			chr = buff.get();
			if(chr!=old_chr) {
				if(chr==0) count++;
				old_chr = chr;
			}
		}
		
		// catch strings
		buff.position(data);
		index=new int[count];
		entrys = new ZeroPaddedString[count];
		for(int i=0; i<count; i++) {
			index[i]=buff.position()-8;
			entrys[i] = new ZeroPaddedString(buff);
		}
		length=entrys.length;

		// push
		pointer.position( pointer.position() + buff.limit());
	}
	
	public MOTX() {
		super(data);
		byte magic[] = { 'X', 'T', 'O', 'M'};
		
		buff.put(magic);	// magic
		buff.putInt(0);		// size
		
		// reset position
		buff.position(0);
	}
	
	
	public void render(){
		if(haschanged){
		int s = 8;
		// calc needed size (+ zero)
		for(int i=0;i<length;i++){
					String ts=entrys[i].toString();
					int temp;
					temp=((s+ts.getBytes().length+1)%4);
					s+=ts.length();
					s++;
					for(int j=0;j<temp+4;j++){
						//System.out.println("Adding Nullbit\t"+j+"\tto entry\t"+i);
						s++;
					}
		}
		//System.out.println(s);
		
		
		// allocate
		ByteBuffer tmp = doRebirth(s);

		// write down
		for(int c=0; c<length; c++) {
					String ts=entrys[c].toString();
					//System.out.println(ts);
					entrys[c].buff.position(0);
					tmp.put(entrys[c].buff);
					tmp.put((byte)0);
					int temp=((tmp.position())%4);
					
					for(int i=0;i<temp+4;i++){
						//System.out.println("Adding Nullbyte\t"+i+"\tto entry\t"+c);
						tmp.put((byte) 0);
					}
					//this is shitty :/
		}
		buff = tmp;
		
		// reset pointers
		writeSize();
		}
		buff.position(0);
	}
	
	/**
	 * Returns the zeropaddedstring at index as normal string
	 * @param index
	 * @return
	 */
	public String getString(int index) {
		if(entrys==null) return null;
		if(index<0 || index>entrys.length) return null;
		return entrys[index].toString();
	}
	
	/**
	 * Adds new string to our filenames list
	 * @return file index of the new string
	 */
	public int addString(String text) {
		int[] indices = new int[length+1];
		ZeroPaddedString tmp[] =  new ZeroPaddedString[length+1];
		for(int c=0; c<length; c++){
			indices[c] = index[c];
			tmp[c] = entrys[c];
		}
		indices[length]=buff.position()-8;
		tmp[length] = new ZeroPaddedString(text);
		
		entrys = tmp;
		index=indices;
		haschanged=true;
		length++;
		writeSize();
		Change();
		return length-1;
	}
	
	/**
	 * Number of strings we have
	 */
	public int getLenght() {
		if(entrys==null) return 0;
		else return entrys.length;
	}
}
