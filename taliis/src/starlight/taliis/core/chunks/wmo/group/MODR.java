package starlight.taliis.core.chunks.wmo.group;

import java.nio.ByteBuffer;

import starlight.taliis.core.chunks.ChunkNotFoundException;
import starlight.taliis.core.chunks.chunk;

/**
 * Doodad references,
 * one 16-bit integer per doodad.
 * 
 * index into MODD of wmo_root
 * 
 * @author Tigurius
 *
 */

public class MODR extends chunk {
	int size;
	int length=0;
	short doodad[];
	boolean haschanged=false;
	
	public MODR(ByteBuffer pointer) throws ChunkNotFoundException {
		super(pointer, "RDOM");

		size = buff.getInt();
		buff.limit(buff.position() + size);
		int temp = size/2;//we have 2bytes per triangle
		doodad=new short[temp];
		for(int i=0;i<temp;i++){
			doodad[i]=buff.getShort();
		}

		
		pointer.position( pointer.position() + buff.limit());
	}
	
	public MODR(){
		super(data);
		byte magic[] = { 'R', 'D', 'O', 'M'};
		
		buff.put(magic);	// magic
		buff.putInt(0);		// size
		
		// reset position
		buff.position(0);	
	}
	
	public int getLength(){
		return length;
	}
	
	public void render(){
		if(haschanged==true){
			ByteBuffer tmp = doRebirth(data + size);
			
			for(int i=0;i<length;i++)
					tmp.putShort(doodad[i]);
			
			buff=tmp;
			writeSize();
			
			buff.position(0);
		}
		else
		buff.position(0);
	}
	
	public short getdoodad(int nr){
		return doodad[nr];
	}
	
	public void setdoodad(int nr, short val){
		doodad[nr]=val;
		haschanged=true;
	}
	
	
	
	public void adddoodad(short index){
		short[] temp=new short[length+1];
		for (int c = 0; c <length; c++)
			temp[c] = doodad[c];
		temp[length]=index;
		doodad=temp;
		length++;
		size+=2;
		writeSize();
		haschanged=true;
	}
}