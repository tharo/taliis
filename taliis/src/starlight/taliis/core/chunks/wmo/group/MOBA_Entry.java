package starlight.taliis.core.chunks.wmo.group;

import java.nio.ByteBuffer;
import starlight.taliis.core.memory;


public final class MOBA_Entry extends memory {
	//not sure about the first 3 byte perhaps color...
	public final static int color1= 0x00;
	public final static int color2 = 0x04;
	public final static int color3= 0x08;
	public final static int startindex = 0x0C;
	public final static int indexnumber = 0x10;
	public final static int startvertex = 0x12;
	public final static int endvertex = 0x14;
	public final static int nullish =0x16;
	public final static int texture =0x17;
	public final static int end=0x18;
	
	public MOBA_Entry(ByteBuffer pointer) {
		super(pointer);
		buff.limit(end);
		pointer.position( pointer.position() + buff.limit() );
	}
	
	public MOBA_Entry(short start,short endv, int startindix, short nIndices){
		super(end);
		buff.putInt(color1, 0xFFE4FFE4);
		buff.putInt(color2, 0x001C0000);
		buff.putInt(color3, 0x0038001C);
		buff.putInt(startindex, startindix);
		buff.putShort(indexnumber, nIndices);
		buff.putShort(startvertex,start);
		buff.putShort(endvertex,endv);
		buff.position(0);
	}
	
	public int getCol1(){
		return buff.getInt(color1);
	}
	public int getCol2(){
		return buff.getInt(color2);
	}
	public int getCol3(){
		return buff.getInt(color3);
	}
	public int getStartIndex(){
		return buff.getInt(startindex);
	}
	public int getIndexNumber(){
		return buff.getInt(indexnumber);
	}
	public int getStartVertex(){
		return buff.getShort(startvertex);
	}
	public int getEndVertex(){
		return buff.getShort(endvertex);
	}
	public int getTexID(){
		return buff.get(texture);
	}
	public void setTexID(byte val){
		buff.put(texture, val);
	}
}