package starlight.taliis.core.chunks.wmo.group;

import java.nio.ByteBuffer;

import starlight.taliis.core.chunks.ChunkNotFoundException;
import starlight.taliis.core.chunks.chunk;

/**
 * Contains Material Information for triangles,
 * 2 bytes per Triangle
 * 
 * @author Tigurius
 *
 */

public class MOPY extends chunk {
	int size;
	byte flags[];
	byte matid[];
	byte newflags[];
	byte newmatid[];
	public int count;
	boolean haschanged;

	public MOPY(ByteBuffer pointer) throws ChunkNotFoundException {
		super(pointer, "YPOM");

		size = buff.getInt();
		buff.limit(data + size);
		count= size/2;
		flags = new byte[count];
		matid = new byte[count];
		for(int i=0;i<count;i++){
			flags[i]=buff.get();
			matid[i]=buff.get();
		}
		
		pointer.position( pointer.position() + buff.limit());
	}
	
	public MOPY(){
		super(data);
		byte magic[] = { 'Y', 'P', 'O', 'M'};
		
		buff.put(magic);	// magic
		buff.putInt(0);		// size
		
		// reset position
		buff.position(0);
	}
	
	public byte getflag(int nr){
		return flags[nr];
	}
	public byte getmatid(int nr){
		return matid[nr];
	}
	
	public void setflag(int nr, byte val, boolean newed){
		if(newed==false)
		flags[nr]=val;
		else
			newflags[nr]=val;
	}
	public void setmatid(int nr, byte val, boolean newed){
		if(newed==false)
			matid[nr]=val;
		else
			newmatid[nr]=val;
	}
	
	public void setnewsize(int val, boolean inject){
		newflags=new byte[val];
		newmatid=new byte[val];
		if(inject==true){
		for(int i=0;i<flags.length;i++)
			newflags[i]=flags[i];
		for(int i=0;i<matid.length;i++)
			newmatid[i]=matid[i];
		}
		count=val;
		size=count*2;
		
		haschanged=true;
	}
	public void render(){
		if(haschanged==true){
		ByteBuffer tmp = doRebirth(data + size);
		for(int i=0;i<count;i++){
			tmp.put(newflags[i]);
			tmp.put(newmatid[i]);
		}
		buff=tmp;
		writeSize();
		
		buff.position(0);
		}
		else buff.position(0);
	}
	
	
}