package starlight.taliis.core.chunks.wmo.group;

import java.nio.ByteBuffer;

import starlight.taliis.core.chunks.ChunkNotFoundException;
import starlight.taliis.core.chunks.chunk;

/**
 * The toplevel MOGP-Chunk of a group file
 * it contains some offsets into the rootfile
 * 
 * @author Tigurius
 *
 */

public class MOGP extends chunk {
	public final static int gName =data+ 0x00; //Group name (offset into MOGN chunk)
	public final static int dName =data+ 0x04;//Descriptive group name (offset into MOGN chunk)
	public final static int flags =data+ 0x08;
	public final static int bb1x = data+ 0xC;
	public final static int bb1y = data+ 0x10;
	public final static int bb1z = data+ 0x14;
	public final static int bb2x = data+ 0x18;
	public final static int bb2y = data+0x1C;
	public final static int bb2z = data+0x20;
	public final static int portalindex = data+0x24;//Index into the MOPR chunk
	public final static int portalcount = data+0x26;//Number of items used from the MOPR chunk
	public final static int batchesA = data+0x28;
	public final static int batchesB = data+0x2A;
	public final static int nMeshes = data+0x2C;//number of MOBA_Entrys
	public final static int fogindices = data+0x30;//4*uint8=index into fog list
	public final static int unknown = data+0x34;//always 15, not always sometime it is null
	public final static int GroupID = data+0x38;
	public final static int Zero1 = data+0x3C;
	public final static int Zero2 = data+0x40;
	public final static int end = data+0x44;
	
	public MOGP(ByteBuffer pointer) throws ChunkNotFoundException {
		super(pointer, "PGOM");

		

		buff.limit(end);
		pointer.position( pointer.position() + buff.limit());
	}
	
	/**
	 * Create new Header -> unimplementated
	 */
	public MOGP() {
		super(end);
		byte magic[] = { 'P', 'G', 'O', 'M'};
		
		buff.put(magic);	// magic
		buff.putInt(unknown, 15);
		buff.putInt(0);		// size
		
		// reset position
		buff.position(0);
	}
	
	public void render(){
		buff.position(0);
	}
	
	/**
	 * As the MOGP-Chunksize is the size of the file-0x15
	 * we have to set it manually;
	 * @param val
	 */
	public void setSize(int val){
		buff.putInt(chunkSize, val);
	}
	
	/**
	 * Returns the offset to the group Name in MOGN
	 * 
	 * @return gName int
	 */
	public int getofsgName(){
		return buff.getInt(gName);
	}
	
	/**
	 * Returns the offset to the descriptive group Name
	 * 
	 * @return dName int
	 */
	public int getofsdName(){
		return buff.getInt(dName);
	}
	public int getFlag(){
		return buff.getInt(flags);
	}
	
	/**
	 * 
	 * @param val
	 */
	public void setFlag(int val){
		buff.putInt(flags, val);
	}
	
	public int getGroupID(){
		return buff.getInt(GroupID);
	}
	
	/**
	 * 
	 * @param val
	 */
	public void setGroupID(int val){
		buff.putInt(GroupID,val);
	}
	
	public int getnMeshes(){
		return buff.getInt(nMeshes);
	}
	public void setnMeshes(int val){
		buff.putInt(nMeshes, val);
	}
	
	
	public void setBoundingBox(float[] bb1,float[] bb2){
		buff.putFloat(bb1x, bb1[0]);
		buff.putFloat(bb1y, bb1[1]);
		buff.putFloat(bb1z, bb1[2]);
		buff.putFloat(bb2x, bb2[0]);
		buff.putFloat(bb2y, bb2[1]);
		buff.putFloat(bb2z, bb2[2]);
	}
	
	public float[][] getBoundingBox(){
		float[][] retfloat=new float[2][3];
		retfloat[0][0]=buff.getFloat(bb1x);
		retfloat[0][1]=buff.getFloat(bb1y);
		retfloat[0][2]=buff.getFloat(bb1z);
		
		retfloat[1][0]=buff.getFloat(bb2x);
		retfloat[1][1]=buff.getFloat(bb2y);
		retfloat[1][2]=buff.getFloat(bb2z);
		return retfloat;
	}
}