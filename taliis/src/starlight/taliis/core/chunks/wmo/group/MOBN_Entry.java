package starlight.taliis.core.chunks.wmo.group;

import java.nio.ByteBuffer;
import starlight.taliis.core.memory;


public final class MOBN_Entry extends memory {
	
	public final static int planetype= 0x00;
	public final static int children1 = 0x02;
	public final static int children2= 0x04;
	public final static int numfaces = 0x06;
	public final static int firstface = 0x08;
	public final static int nunk = 0x0A;
	public final static int fDist = 0x0C;
	public final static int end=0x10;
	
	public MOBN_Entry(ByteBuffer pointer) {
		super(pointer);
		buff.limit(end);
		pointer.position( pointer.position() + buff.limit() );
	}
	public MOBN_Entry(int nfaces, int firstfce){
		super(end);
		buff.putShort(planetype, (short) 4);
		buff.putShort(children1, (short) -1);
		buff.putShort(children2, (short) -1);
		buff.putInt(numfaces, nfaces);
		buff.putInt(firstface, firstfce);
		buff.position(0);
	}
	public void render(){
		buff.position(0);
	}
	
	public void setnTriangles(int val){
		buff.putShort(numfaces,(short)val);
	}
	public int getnTriangles(){
		return buff.getShort(numfaces);
	}
	public void setFirstFace(int val){
		buff.putShort(firstface,(short) val);
	}
	public int getFirstFace(){
		return buff.getShort(firstface);
	}
	
}