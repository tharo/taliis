package starlight.taliis.core.chunks.wmo.group;

import java.nio.ByteBuffer;

import starlight.taliis.core.chunks.ChunkNotFoundException;
import starlight.taliis.core.chunks.chunk;

/**
 *Normals. 3 floats per vertex normal, 
 *in (X,Z,-Y) order..
 *
 * @author Tigurius
 *
 */

public class MONR extends chunk {
	int size;
	float[][] index;
	float[][] newindex;
	public int nNormals;
	boolean haschanged;
	
	public MONR(ByteBuffer pointer) throws ChunkNotFoundException {
		super(pointer, "RNOM");

		size = buff.getInt();
		buff.limit(data + size);
		nNormals = size/12;//we have 3*4bytes per triangle
		
		index= new float[nNormals][3];
		for(int i=0;i<nNormals;i++){
			for(int j=0;j<3;j++)
				index[i][j]=buff.getFloat();
		}
		
		pointer.position( pointer.position() + buff.limit());
	}
	
	public MONR(){
		super(data);
		byte magic[] = { 'R', 'N', 'O', 'M'};
		
		buff.put(magic);	// magic
		buff.putInt(0);		// size
		
		// reset position
		buff.position(0);
	}
	
	/**
	 * Returns the Position of the given normal
	 * 
	 * @param nr
	 * @return pos float[3]
	 */
	public float[] getNormalPos(int nr){
		return index[nr];
	}
	
	/**
	 * Sets the Position of the given Normal
	 * 
	 * @param nr int
	 * @param val float[3]
	 */
	public void setNormalPos(int nr, float[] val){
		index[nr]=val;
		
	}
	
	/**
	 * Returns the Position of the given normal
	 * ::to use if size has changed
	 * @param nr
	 * @return pos float[3]
	 */
	public float[] getnewNormalPos(int nr){
		return newindex[nr];
	}
	
	/**
	 * Sets the Position of the given Normal
	 * ::to use if size has changed
	 * @param nr int
	 * @param val float[3]
	 */
	public void setnewNormalPos(int nr, float[] val){
		newindex[nr]=val;
		
	}
	
	/**
	 * Sets the capacity of the MOVT-chunk to the given value
	 * 
	 * @param val
	 */
	public void setnewSize(int val, boolean inject){
		newindex=new float[val][3];
		if(inject==true)
		for(int i=0;i<index.length;i++)
			newindex[i]=index[i];
		nNormals=val;
		size=nNormals*12;
		
		haschanged=true;
	}
	
	public void render(){
		if(haschanged==true){
		ByteBuffer tmp = doRebirth(data + size);
		
		for(int i=0;i<nNormals;i++)
			for(int j=0;j<3;j++)
				tmp.putFloat(newindex[i][j]);
		
		buff=tmp;
		writeSize();
		
		buff.position(0);
		}
		else buff.position(0);
	}
}
	