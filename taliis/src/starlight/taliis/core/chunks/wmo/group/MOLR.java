package starlight.taliis.core.chunks.wmo.group;

import java.nio.ByteBuffer;

import starlight.taliis.core.chunks.ChunkNotFoundException;
import starlight.taliis.core.chunks.chunk;

/**
 * Light references, 
 * one 16-bit integer per light reference.
 * 
 * @author Tigurius
 *
 */

public class MOLR extends chunk {
	int size;
	short light[];
	public int count;

	public MOLR(ByteBuffer pointer) throws ChunkNotFoundException {
		super(pointer, "RLOM");

		size = buff.getInt();
		buff.limit(buff.position() + size);
		count = size/2;//we have 2bytes per triangle
		light=new short[count];
		for(int i=0;i<count;i++){
			light[i]=buff.getShort();
		}
		
		pointer.position( pointer.position() + buff.limit());
	}
	
	public short getlight(int nr){
		return light[nr];
	}
}