package starlight.taliis.core.chunks.wmo.group;

import java.nio.ByteBuffer;

import starlight.taliis.core.chunks.ChunkNotFoundException;
import starlight.taliis.core.chunks.chunk;

/**
 * Vertex indices for triangles.
 * Three 16-bit integers per triangle,
 * that are indices into the vertex list. 
 * The numbers specify the 3 vertices for each triangle,
 * their order makes it possible to do backface culling. 
 * 
 * @author Tigurius
 *
 */

public class MOVI extends chunk {
	int size;
	short index[][];
	short newindex[][];
	public int nTriangles;
	boolean haschanged;
	
	public MOVI(ByteBuffer pointer) throws ChunkNotFoundException {
		super(pointer, "IVOM");

		size = buff.getInt();
		buff.limit(data + size);
		nTriangles= size/6;//we have 3*2bytes per triangle
		index= new short[nTriangles][3];
		
		for(int i=0;i<nTriangles;i++){
			for(int j=0;j<3;j++)
				index[i][j]=buff.getShort();
		}
		
		pointer.position( pointer.position() + buff.limit());
	}
	
	public MOVI(){
		super(data);
		byte magic[] = { 'I', 'V', 'O', 'M'};
		
		buff.put(magic);	// magic
		buff.putInt(0);		// size
		
		// reset position
		buff.position(0);
	}
	
	
	/**
	 * Returns the Indices for the Vertices of a Triangle
	 * 
	 * @param nr int
	 * @return index short[3]
	 */
	public short[] getIndex(int nr){
		return index[nr];
	}
	
	/**
	 * Sets the Indices for the Vertices of a Triangle
	 * 
	 * @param nr int
	 * @param val short[3]
	 */
	public void setIndex(int nr, short[] val){
		index[nr]=val;
		
	}
	
	/**
	 * Returns the Indices for the Vertices of a Triangle
	 * 
	 * @param nr int
	 * @return index short[3]
	 */
	public short[] getnewIndex(int nr){
		return newindex[nr];
	}
	
	/**
	 * Sets the Indices for the Vertices of a Triangle
	 * 
	 * @param nr int
	 * @param val short[3]
	 */
	public void setnewIndex(int nr, short[] val){
		newindex[nr]=val;
		
	}
	/**
	 * Sets the capacity of the MOVT-chunk to the given value
	 * 
	 * @param val
	 */
	public void setnewSize(int val, boolean inject){
		newindex=new short[val][3];
		if(inject==true)
		for(int i=0;i<index.length;i++)
			newindex[i]=index[i];
		nTriangles=val;
		size=nTriangles*6;
		haschanged=true;
	}
	
	public void render(){
		if(haschanged==true){
		ByteBuffer tmp = doRebirth(data + size);
		
		for(int i=0;i<nTriangles;i++)
			for(int j=0;j<3;j++)
				tmp.putShort(newindex[i][j]);
		
		buff=tmp;
		writeSize();
		
		buff.position(0);
		}
		else buff.position(0);
	}
}