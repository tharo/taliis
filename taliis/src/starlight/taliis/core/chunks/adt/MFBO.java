package starlight.taliis.core.chunks.adt;

import java.nio.ByteBuffer;

import starlight.taliis.core.chunks.ChunkNotFoundException;
import starlight.taliis.core.chunks.chunk;

/**
 * Flight boundarys (TBC)
 * Infos taken from:
 * 		http://madx.dk/wowdev/wiki/index.php?title=ADT#MFBO_chunk
 * 
 * At the moment this chunk is NOT implementated! 
 * @author ganku
 *
 */

public class MFBO  extends chunk {
	int size;
	
	public MFBO(ByteBuffer pointer) throws ChunkNotFoundException {
		super(pointer, "OBFM");

		size = buff.getInt();

		buff.limit(buff.position() + size);
		pointer.position( pointer.position() + buff.limit());
	}
	
	/**
	 * No creation routine done atm !!
	 */
	public MFBO() {
		super(data);
		
		byte magic[] = { 'O', 'B', 'F', 'M'};	
		buff.put(magic);		// magic
		buff.putInt(0);			// size
		
		buff.position(0);
	}
}
