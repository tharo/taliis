package starlight.taliis.core.chunks.adt;



import java.nio.*;

import starlight.taliis.core.memory;
import starlight.taliis.core.chunks.ChunkNotFoundException;
import starlight.taliis.core.chunks.chunk;

/**
 * New Water-Chunk in WotLk, replaces the old MCLQ-Chunk
 * Should look like this, but not sure if done correctly xD
 * @author tigurius
 */

public final class MH2O extends chunk {
	public MH2O_Entry info[];
	public MH2O_Data1 offDat1[];
	public Data2 Dat2[];
	public Data2a	Dat2a[];
	public Data2b Dat2b[];
	int size;
	int temp;
	
	public final static int HeaderandData1size = 0x2400;
	
	/*
	 * 0x0	magic
	 * 0x4	size
	 * 0x8	data
	 */
	public MH2O(ByteBuffer pointer) throws ChunkNotFoundException {
		super(pointer, "O2HM");
		
		size = buff.getInt();
		
		// size + magic
		buff.limit(buff.position()+size);
		
		// read header data
		info = new MH2O_Entry[256];
		for(int c=0; c<256; c++)
			info[c] = new MH2O_Entry(buff);
		
		// read data1
		offDat1 = new MH2O_Data1[256];
		for(int c=0; c<256; c++)
		{	if(info[c].GetoffsData1()!=0){
			buff.position(info[c].GetoffsData1());
			offDat1[c] = new MH2O_Data1(buff);
			}
		else offDat1[c]=null;
		}
		
		//read Data2a; defines which of the quads should be rendered
		//and Data2b which contains (width*height)float-values for height-map 
		temp = 0;
		Dat2a = new Data2a[256];
		Dat2b = new Data2b[256];
		Dat2 = new Data2[256];
		for(int i=0;i<256;i++){

			if(info[i].GetoffsData2()!=0)
			{buff.position(info[i].GetoffsData2());
			Dat2[i]= new Data2(buff, true);}
			else
				Dat2[i]= null;
			if(offDat1[i]!=null){
			if(offDat1[i].GetoffsData2a()!=0){
				temp= offDat1[i].Getheight();
				buff.position(offDat1[i].GetoffsData2a());
				Dat2a[i]= new Data2a(buff, temp);
			}
			//Data2a doesn't exist -->all is rendered
			else Dat2a[i]= null;
			
			if(offDat1[i].GetoffsData2b()!=0){
				buff.position(offDat1[i].GetoffsData2b());
				Dat2b[i]= new Data2b(buff, (offDat1[i].Getheight()*offDat1[i].Getwidth()));	
			}
			else Dat2b[i]= null;
			}	
			else
			{
				Dat2a[i]= null;
				Dat2b[i]= null;
			}
		}
		
		
		// push pointers
		pointer.position( pointer.position() + buff.limit());
	}
	
	public MH2O() {
		super(data+HeaderandData1size);
		byte magic[] = { 'O', '2', 'H', 'M'};
		
		buff.put(magic);					// put magic
		buff.putInt(HeaderandData1size);	// put size
		
		//header data
		info = new MH2O_Entry[256];
		for(int c=0; c<256; c++){
			info[c] = new MH2O_Entry(buff);
			info[c].SetOffsData1((256*0xC)+c*0x18);
			}
		
		// data1
		offDat1 = new MH2O_Data1[256];
		for(int c=0; c<256; c++)
			offDat1[c] = new MH2O_Data1(buff);
		
		// reset position
		buff.position(0);	
		
		if(DEBUG) System.out.println("Empty MH2O created.");
	}
	
	/*public void render(){
		int s = 0;
		for(int c=0; c<256; c++)
			s+=0xC;
		for(int c=0;c<256;c++)
			if(info[c].GetoffsData1()!=0)
			s+=0x18;
		for(int c=0;c<256;c++)
			if(info[c].GetoffsData2()!=0)
			s+=Dat2[c].getSize();
		for(int c=0;c<256;c++)
			if(offDat1[c].GetoffsData2a()!=0)
			s+=Dat2a[c].getSize();
		for(int c=0;c<256;c++)
			if(offDat1[c].GetoffsData2b()!=0)
			s+=Dat2b[c].getSize();
		
		ByteBuffer tmp = this.doRebirth( s + data );
		
		tmp.position(magic);
		byte magic[] = { 'O', '2', 'H', 'M'};
		tmp.put(magic);
		writeSize();
		buff.position(0);
		//I didn't get the point ._.
	}*/
}

class Data2a extends memory{
	Data2a (ByteBuffer pointer, int val){
		super(pointer);
		
		buff.limit(val);
		pointer.position(pointer.position() + buff.limit());
	}
}

class Data2b extends memory{
	Data2b (ByteBuffer pointer, int val){
		super(pointer);
		
		buff.limit(val);
		pointer.position(pointer.position() + buff.limit());
	}
}

class Data2 extends memory{
	Data2 (ByteBuffer pointer, boolean is){
		super(pointer);
		if(is==true)
		buff.limit(0x10);
		else buff.limit(0);
		pointer.position(pointer.position() + buff.limit());
	}

}


