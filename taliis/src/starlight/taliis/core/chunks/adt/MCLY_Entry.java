package starlight.taliis.core.chunks.adt;

import java.nio.ByteBuffer;
import starlight.taliis.core.*;

/**
 * Holds the texture infomations
 * @author conny
  *
 *@author updated by tigurius
 *
 */
public final class MCLY_Entry extends memory {	
	/**
	 * An texture that is assigned to this layer.
	 */
	/*000h*/  public final static int textureId = 0x0;
	/**
	 * Flags for this layer.
	 */
	/*004h*/  public final static int props = 0x4;		
	 /**
	 * The offset to the alphamaps in MCAL
	 */
	/*008h*/  public final static int offsAlpha = 0x8;		
	/**
	 * A set of doodads assigned in GroundEffectDoodad.dbc. Small plants etc.
	 */
	/*00Ch*/  public final static int effectId = 0xc;		
	/*010h*/  public final static int end = 0x10;
	
	MCLY_Entry(ByteBuffer pointer) {
		super(pointer);
		
		buff.limit(end);
		pointer.position( pointer.position() + buff.limit());
	}
	
	/**
	 * Create new entry
	 */
	MCLY_Entry() {
		super(end);
		// set default flag
		buff.putInt(props, MCLY.MCLY_USE_DEFAULT);
		
		// reset position
		buff.position(0);
		
		if(DEBUG) System.out.println("    New MCLY Layer created.");
	}
	
	/**
	 * 
	 * @return The AlphamapID to be used
	 */ 
	public int getAlphaID(){
		return buff.getInt(offsAlpha)/0x800;
	}
	
	public void setAlphaID(int id){
		buff.putInt(offsAlpha, id*0x800);
	}
	
	/**
	 * @param val The new assigned texture.
	 */
	public void setTextureID(int val) {
		buff.putInt(textureId, val);
	}
	/**
	 * @return The assigned texture.
	 */
	public int getTextureID() {
		return buff.getInt(textureId);
	}
	/**
	 * @param val New flags. See MCLY.MCLY_USE_*.
	 */
	public void setFlags(int val) {
		buff.putInt(props, val);
	}
	/**
	 * @return Get the flags. See MCLY.MCLY_USE_*.
	 */
	public int getFlags() {
		return buff.getInt(props);
	}
	/**
	 * @param val New ground-effect-doodads. Defined in GroundEffectDoodad.dbc.
	 */
	public void setGroundEffectID(int val) {
		buff.putInt(effectId, val);
	}
	/**
	 * @return Ground-effect-doodads. Defined in GroundEffectDoodad.dbc.
	 */
	public int getGroundEffectID() {
		return buff.getInt(effectId);
	}
}