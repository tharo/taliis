package starlight.taliis.core.chunks.adt;
/**
 *  MTEX chunk
 *  List of textures used by the terrain in this map tile.
 *   A contiguous block of zero-terminated strings,
 *   that are complete filenames with paths. 
 *   The textures will later be identified by their position
 *   in this list. 
 */

import java.nio.*;
import starlight.taliis.core.ZeroTerminatedString;
import starlight.taliis.core.chunks.ChunkNotFoundException;
import starlight.taliis.core.chunks.chunk;


public final class MTEX extends chunk {	
	public ZeroTerminatedString entrys[];
	int lenght = 0;
	
	public MTEX(ByteBuffer pointer) throws ChunkNotFoundException {
		super(pointer, "XETM");
		
		// get size
		int size = buff.getInt();
		buff.limit( size + data );
		
		// count 0 (string endings)
		while(buff.hasRemaining()) {
			byte t = buff.get();
			if(t==0) lenght++;
		}
		
		// init strings
		entrys = new ZeroTerminatedString[lenght];
		buff.position(data);
		for(int i=0; i<lenght; i++) {
			entrys[i] = new ZeroTerminatedString(buff);
		}
		
		// push pointer
		pointer.position( pointer.position() + buff.limit() ); 
	}
	
	public MTEX() {
		super(0x8);
		byte magic[] = { 'X', 'E', 'T', 'M'};
		
		buff.put(magic);	// magic
		buff.putInt(0);		// size
		
		// reset position
		buff.position(0);	
		
		if(DEBUG) System.out.println("Empty MTEX created.");
	}
	
	/**
	 * render data to write it back to a file
	 */
	public void render() {
		int s = 0;
		// calc needed size (+ zero)
		for(int c=0; c<lenght; c++)
			s+=entrys[c].getLenght()+1;
		
		// allocate
		ByteBuffer tmp = doRebirth(s + data);
		
		// write down
		for(int c=0; c<lenght; c++) {
			entrys[c].buff.position(0);
			tmp.put( entrys[c].buff );
			tmp.put((byte)0);
		}
		buff = tmp;
		
		// reset pointers
		writeSize();
		buff.position(0);
	}
	
	
	/**
	 * Adds new string to our filenames list
	 * @return file index of the new string
	 */
	public int addString(String text) {
		ZeroTerminatedString tmp[] =  new ZeroTerminatedString[lenght+1];
		for(int c=0; c<lenght; c++)
			tmp[c] = entrys[c];
		tmp[lenght] = new ZeroTerminatedString(text);
		
		entrys = tmp;
		lenght++;
		Change();
		return lenght-1;
	}
	
	/**
	 * Removes the String with the given index.
	 * WARNING!!! This can cause RAPID display failures by
	 * re-ordered indexies !!
	 * @param index
	 */
	public void removeString(int index) {
		// make new array of objects
		ZeroTerminatedString tmp[] =  new ZeroTerminatedString[lenght-1];
		
		// copy data in front of killed item
		for(int c=0; c<index; c++) {
			tmp[c]=entrys[c];
		}
		
		// copy data in front of killed item
		for(int c=index+1; c<lenght; c++) {
			tmp[c-1]=entrys[c];
		}
		
		// remove old references
		entrys = tmp;

		// set new number
		lenght--;
		
		// we have a new size now
		Change();
	}
	
	/**
	 * get the texture value
	 * @param number index in the string array
	 * @return the texture filename
	 */
	public String getValueNo(int index) {
		if(index>lenght) return null;
		else return entrys[index].toString();
	}
	
	/**
	 * @return number of strings in this file
	 */
	public int getLenght() {
		return lenght;
	}
}

