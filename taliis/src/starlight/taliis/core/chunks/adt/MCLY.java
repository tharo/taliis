package starlight.taliis.core.chunks.adt;

import java.nio.ByteBuffer;

import starlight.taliis.core.chunks.ChunkNotFoundException;
import starlight.taliis.core.chunks.chunk;

/**
 * Texture layer definitions for this map chunk. <br>
 * 16 bytes per layer, up to 4 layers.
 * 
 * @author conny 
 * @author updated by tigurius
 * 
 */

public final class MCLY extends chunk {

	/**
	 * Default flag for layers without special things.
	 */
	public final static int MCLY_USE_DEFAULT = 128;
	/**
	 * Used for layers with alphamaps (all but the first one.
	 */
	public final static int MCLY_USE_ALPHAMAPS = 256;
	/**
	 * Only used in WotLK for new, compressed alphamaps.
	 */
	public final static int MCLY_USE_COMPRESSED_ALPHAMAPS = 512;

	int length = 0;
	/**
	 * The layers themselfes.
	 */
	public MCLY_Entry layer[];

	/*
	 * 0x0 magic 
	 * 0x4 size ? 
	 * 0x8 data 
	 * oxc effectID
	 */

	/**
	 * Create this chunk from a buffer.
	 * 
	 * @param pointer A pointer to this chunk.
	 * @throws ChunkNotFoundException If the pointer is not pointing to a MCLY
	 *             chunk.
	 */
	public MCLY(ByteBuffer pointer) throws ChunkNotFoundException {
		super(pointer, "YLCM");

		// number of entrys?
		length = buff.getInt() / MCLY_Entry.end;
		layer = new MCLY_Entry[4];

		for (int c = 0; c < length; c++)
			layer[c] = new MCLY_Entry(buff);

		// pointer stuff ...
		buff.limit(buff.position());
		pointer.position(pointer.position() + buff.limit());
	}

	/**
	 * Create a new and empty chunk.
	 */
	public MCLY() {
		super(data);
		byte magic[] = { 'Y', 'L', 'C', 'M' };

		buff.put(magic); // magic
		buff.putInt(0); // size

		// create layers (may count)
		layer = new MCLY_Entry[4];

		// reset position
		buff.position(0);

		if (DEBUG)
			System.out.println("  MCLY created.");
	}

	// TODO: render all my layer datas
	// TODO: also add render(alpha offset) later
	@Override
	public void render() {
		// create new buffer
		ByteBuffer tmp = doRebirth(data + length * MCLY_Entry.end);

		// copy data
		for (int c = 0; c < length; c++)
			tmp.put(layer[c].buff);

		// replace old buffer with new one
		buff = tmp;

		// update size entry
		writeSize();

		buff.position(0);
	}

	/**
	 * Init a new layer entry with some data for me :D
	 * 
	 * @param texture_id The new id of the texture in MTEX.
	 */
	public void add(int texture_id,int ground_id) {
		if (length == 4)
			return;
		MCLY_Entry[] tmp=new MCLY_Entry[length+1];
		for(int i=0;i<length;i++){
			tmp[i]=layer[i];
		}
		tmp[length] = new MCLY_Entry();
		tmp[length].setTextureID(texture_id);
		if(length>1){
			tmp[length].setFlags(MCLY_USE_ALPHAMAPS);
			tmp[length].setGroundEffectID(ground_id);
			tmp[length].setAlphaID(length-1);
		}
		layer=tmp;
		length++;
	}

	/**
	 * Get the assigned texture of a layer.
	 * 
	 * @param lay Which layer.
	 * @return The texture-id in MTEX for this layer.
	 */
	public int GetTexID(int lay) {
		return layer[lay].getTextureID();
	}

	/**
	 * Set the assigned texture of a layer.
	 * 
	 * @param val The new textureid.
	 * @param layer_ The layer you want to change.
	 */
	public void SetTexId(int val, int layer_) {
		layer[layer_].setTextureID(val);
	}

	/**
	 * @param layer_ The layer.
	 * @return Returns the ground effect doodads' id.
	 */
	public int GetGroundEffect(int layer_) {
		return layer[layer_].getGroundEffectID();
	}

	/**
	 * Sets the ground effect doodads' id.
	 * 
	 * @param val The doodads' id in GroundEffectDoodads.dbc.
	 * @param layer_ The layer.
	 */
	public void SetGroundEffect(int val, int layer_) {
		layer[layer_].setGroundEffectID(val);
	}

	/**
	 * Returns a layer out of this chunk.
	 * 
	 * @param layer Which layer.
	 * @return The MCLY entry.
	 */
	public MCLY_Entry getLayer(int layer) {
		if (layer < 0 || layer > 3)
			return null;
		else
			return this.layer[layer];
	}

	/**
	 * @return numver of layers currently init.
	 * @deprecated Spelling. Use getLength().
	 */
	@Deprecated
	@Override
	public int getLenght() {
		return length;
	}

	/**
	 * @return numver of layers currently init.
	 */
	@Override
	public int getLength() {
		return length;
	}
}