package starlight.taliis.core.chunks.adt;

/**
 * Normal vectors for each vertex, encoded as 3 signed bytes per normal, in the same order as specified above.
 * The size field of this chunk is wrong!
 * Actually, it isn't wrong, but there are a few junk (unknown?) 
 * bytes between the MCNR chunk's end and the next chunk. 
 * The size of the MCNR chunk should be adjusted to 0x1C0 bytes 
 * instead of 0x1B3 bytes. 
 * ... 
 * 
 * @author conny
 *
 */

import java.nio.*;

import starlight.taliis.core.chunks.ChunkNotFoundException;
import starlight.taliis.core.chunks.chunk;


public final class MCNR extends chunk {
	int size;
	
	/*
	 * 0x0	magic
	 * 0x4	size
	 * 0x8	data
	 */
	public final static int probSize = 0x1C0;
	
	
	MCNR(ByteBuffer pointer) throws ChunkNotFoundException {
		super(pointer, "RNCM");

		size = buff.getInt();
		// DONT trust the size field!
		buff.limit(buff.position() + probSize);
		pointer.position( pointer.position() + buff.limit());
	}
	
	public MCNR() {
		super(data + probSize);
		byte magic[] = { 'R', 'N', 'C', 'M'};
			
		buff.put(magic);		// magic
		buff.putInt(probSize);	// size
		
		// reset position
		buff.position(0);	
		
		if(DEBUG) System.out.println("  MCNR created.");
	}
	
	public double getXNorm(int index) {
		byte tmp = buff.get(data+3*index);
		
		double res = (double)1/127*tmp;
		return res;
	}
	public double getZNorm(int index) {
		byte tmp = buff.get(data+3*index+1);
		
		double res = (double)1/127*tmp;
		return res;
	}
	public double getYNorm(int index) {
		byte tmp = buff.get(data+3*index+2);
		
		double res = (double)1/127*tmp;
		return res;
	}
	public byte[] getNorm(int index) {
		byte[] ret = new byte[3];
		buff.position(data+3*index);
		buff.get(ret);
		return ret;
	}
	public byte[] getValNoLOD(int x, int y) {
		byte[] ret = new byte[3];
		int add = y*8;
		int index = (y*9 + x + add);
		
		buff.position(data+3*index);
		buff.get(ret);
		return ret;
	}
}