package starlight.taliis.core.chunks.adt;

/**
 * Alpha map. One layer for each texture layer 
 * size MAY be wrong on fields with no textures!
 * 
 * @author tharo
 *
 */

import java.nio.*;

import starlight.taliis.core.chunks.ChunkNotFoundException;
import starlight.taliis.core.chunks.chunk;

public final class MCAL extends chunk {
	int size, lenght;
	public MCAL_Entry layers[] = null;
	
	MCAL(ByteBuffer pointer) throws ChunkNotFoundException  {
		super(pointer, "LACM");

		size = buff.getInt();
		//TODO: size may be wrong!
		lenght = size / MCAL_Entry.end;
		
		if(lenght>0) {
			layers = new MCAL_Entry[lenght];
			for(int c=0; c<lenght; c++) {
				layers[c] = new MCAL_Entry(buff);
			}
		}
		
		buff.limit(buff.position());
		pointer.position( pointer.position() + buff.limit());
	}
	
	/**
	 * Creates a new MCAL layer.
	 * 
	 * At the moment this is only an empty chunk with no data
	 * and a size of 0. This have to be changed of course ^.^
	 */
	public MCAL() {
		super(data);
		byte magic[] = { 'L', 'A', 'C', 'M'};
			
		buff.put(magic);	// magic
		buff.putInt(0);		// size
		
		// reset position
		buff.position(0);
		
		if(DEBUG) System.out.println("  MCAL created.");
	}
	
	/**
	 * Rebuild the whole thing
	 */
	public void render() {
		// nothing to render?
		if(layers==null) {
			buff.position(0);
			return;
		}
		
		//rebirth
		ByteBuffer tmp = this.doRebirth(data + layers.length*MCAL_Entry.end);

		//copy all data
		for(int c=0; c<layers.length; c++) {
			layers[c].render();
			tmp.put(layers[c].buff);
		}

		// switch
		buff = tmp;
		
		// we have a new size now
		writeSize();
		
		//TODO: Rein
		
		// reset position
		buff.position(0);
	}
	
	/**
	 * Returns layer no n - if exist
	 * @param n
	 * @return
	 */
	public MCAL_Entry getLayerNo(int n) {
		if(n<0 || n>=layers.length) return null;
		return layers[n];
	}
	
	/**
	 * creates a new alpha layer. it will
	 * get the next free position
	 * 
	 * WARNING: this may cause ADT errors if you have more/less
	 * alpha layers than texture layers!
	 * 
	 * @return new created layer or null
	 */
	public MCAL_Entry createLayer() {
		if(layers==null) {
			MCAL_Entry layers[] = new MCAL_Entry[1];
			layers[0] = new MCAL_Entry();
			return layers[0];
		}
		
		int s = layers.length;
		if(s>=3) return null;
		
		MCAL_Entry newLayers[] = new MCAL_Entry[s+1];
		for(int c=0; c<s; c++)
			newLayers[c]=layers[c];
		
		newLayers[s] = new MCAL_Entry();
		layers = newLayers;
		
		return layers[s];
	}
	
	/**
	 * Will remove the top layer 
	 * 
	 * WARNING: this may cause ADT errors if you have more/less
	 * alpha layers than texture layers!
	 * 
	 */
	public void killTopLayer() {
		int s = layers.length;
				
		MCAL_Entry newLayers[] = new MCAL_Entry[s-1];
		
		for(int c=0; c<(s-1); c++)
			newLayers[c]=layers[c];
		
		layers = newLayers;
	}
	
	/**
	 * return number of layers we do have
	 * @return number of layers
	 */
	public int getLenght() {
		return getSize()/ MCAL_Entry.end;
	}
}