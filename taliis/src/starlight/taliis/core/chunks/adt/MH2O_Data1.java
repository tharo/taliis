package starlight.taliis.core.chunks.adt;

import java.nio.ByteBuffer;
import starlight.taliis.core.memory;

public final class MH2O_Data1 extends memory {
	/*0x00*/	public final static short flags = 0x0;
	/*0x02*/	public final static short type = 0x2;
	/*0x04*/	public final static float hLevel1 = 0x4;
	/*0x08*/	public final static float hLevel2 = 0x8;
	/*0x0C*/	public final static byte xOffs = 0xC;
	/*0x0D*/	public final static byte yOffs = 0xD;
	/*0x0E*/	public final static byte width = 0xE;
	/*0x0F*/	public final static byte height = 0xF;
	/*0x10*/	public final static int offsData2a = 0x10;
	/*0x14*/	public final static int offsData2b = 0x14;
	/*0x18*/	public final static int end = 0x18;
	
	public MH2O_Data1(ByteBuffer pointer) {
		super(pointer);
		buff.limit(end);
		pointer.position( pointer.position() + buff.limit() );
	}
	
	public short Getflags(){
		return buff.getShort(flags);
	}
	
	public void Setflags(short val){
		buff.putShort(flags, val);
	}
	
	public short Gettype(){
		return buff.getShort(type);
	}
	
	public void Settype(short val){
		buff.putShort(type, val);
	}
	public float GethLevel1(){
		return buff.getFloat((int) hLevel1);
	}
	
	public void SethLevel1(float val){
		buff.putFloat((int) hLevel1, val);
	}
	public float GethLevel2(){
		return buff.getFloat((int) hLevel2);
	}
	
	public void SethLevel2(float val){
		buff.putFloat((int) hLevel2, val);
	}
	public byte Getxoffs(){
		return buff.get(xOffs);
	}
	
	public void SetxOffs(byte val){
		buff.put(xOffs, val);
	}
	public byte Getyoffs(){
		return buff.get(yOffs);
	}
	
	public void SetyOffs(byte val){
		buff.put(yOffs, val);
	}
	public byte Getwidth(){
		return buff.get(width);
	}
	
	public void Setwidth(byte val){
		buff.put(width, val);
	}
	public byte Getheight(){
		return buff.get(height);
	}
	
	public void Setheight(byte val){
		buff.put(height, val);
	}
	public int GetoffsData2a(){
		return buff.getInt(offsData2a);
	}
	
	public void SetOffsData2a(int val){
		buff.putInt(offsData2a, val);
	}
	public int GetoffsData2b(){
		return buff.getInt(offsData2b);
	}
	
	public void SetOffsData2b(int val){
		buff.putInt(offsData2b, val);
	}

	
}