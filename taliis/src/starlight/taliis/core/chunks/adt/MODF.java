package starlight.taliis.core.chunks.adt;

/**
 * Placement information for WMOs. 64 bytes per wmo instance.
 * @author conny
 *
 */

import java.nio.*;

import starlight.taliis.core.chunks.ChunkNotFoundException;
import starlight.taliis.core.chunks.chunk;


public final class MODF extends chunk {	
	private int lenght = 0;
	public MODF_Entry entrys[];
	
	/*
	 * 0x0	magic key
	 * 0x4	data size
	 * 0x8	data
	 */

	/**
	 * @param pointer Data Buffer
	 * @param ModelCount Number of models we expect 
	 */
	public MODF(ByteBuffer pointer) throws ChunkNotFoundException {
		super(pointer, "FDOM");
		
		// get number of entrys
		int size = buff.getInt();
		lenght = size / MODF_Entry.end;
		
		// init them
		entrys = new MODF_Entry[lenght];
		for(int c=0; c<lenght; c++)
			entrys[c] = new MODF_Entry(buff);
		
		// setup the buffer stuff ...
		buff.limit( data + size );
		pointer.position( pointer.position() + buff.limit() );
	}
	

	public MODF() {
		super(data);
		byte magic[] = { 'F', 'D', 'O', 'M'};
			
		buff.put(magic);	// magic
		buff.putInt(0);		// size
		 
		// reset position
		buff.position(0);
		
		if(DEBUG) System.out.println("Empty MODF created.");
	}

	
	/**
	 * Adds the object given by strIndex to the given coordinates
	 * @param strIndex	index of the loaded file in our table
	 * @param uID		unique index in whole instance
	 * @param x			x position
	 * @param y			y position
	 * @param z			z position
	 * @return			index of created object in the MODF list
	 */
	public int addObject(int strIndex, int uID, float x, float y, float z) {
		//TODO: look first for free objects
		
		// make new array of objects
		MODF_Entry tmp[] = new MODF_Entry[lenght +1];
		
		// copy all data
		for(int c=0; c<lenght; c++) {
			tmp[c]=entrys[c];
		}
		
		// remove old references
		entrys = tmp;
		
		// create newest entry
		entrys[lenght] = new MODF_Entry(strIndex, uID, x, y, z);
		
		// set new number
		lenght++;
		
		// we have a new size now
		Change();
		
		return lenght -1;
	}
	
	/**
	 * A basic function that removes entry index
	 * and rebuilds the list
	 * @param index
	 */
	public void remove(int index) {
		// make new array of objects
		MODF_Entry tmp[] = new MODF_Entry[lenght -1];
		
		// copy data in front of killed item
		for(int c=0; c<index; c++) {
			tmp[c]=entrys[c];
		}
		
		// copy data in front of killed item
		for(int c=index+1; c<lenght; c++) {
			tmp[c-1]=entrys[c];
		}
		
		// remove old references
		entrys = tmp;

		// set new number
		lenght--;
		
		// we have a new size now
		Change();		
	}
	
	/**
	 * Allocate our memory .. render all sub-objects
	 */
	public void render() {
		// calculate out size
		int l = 0;
		
		// do we had any changes?
		//if(isChanged()) {
			// re - create all memory
			
			// check for existence
			l = getLenght();
			
			System.out.println(l + " object appeareances.");
			
			// create new buffer
			ByteBuffer tmp = ByteBuffer.allocate( l*MODF_Entry.end + data );
			tmp.order(ByteOrder.LITTLE_ENDIAN);
			
			// add magic
			tmp.position(magic);
			byte magic[] = { 'F', 'D', 'O', 'M'};
			tmp.put(magic);
			
			// copy all data
			tmp.position(data);
			for(int c=0; c<lenght; c++)
				if(entrys[c]!=null) {
					entrys[c].buff.position(0);
					tmp.put( entrys[c].buff );
				}
					
			// kick out old memory
			buff = tmp;
			
			// write new size
			writeSize();
			
			// re init out data objects
			buff.position(0x8);
			
			entrys = new MODF_Entry[l];
			for(int c=0; c<l; c++)
				entrys[c] = new MODF_Entry(buff);
			
		//}
		
		buff.position(0);
	}
	
	/**
	 * @return number of objects stored inside
	 */
	public int getLenght() {
		int l = 0;
		for(int c=0; c<lenght; c++)
			if(entrys[c]!=null) l++;

		// well .. just to make sure 
		lenght = l;
		return l;
	}
}