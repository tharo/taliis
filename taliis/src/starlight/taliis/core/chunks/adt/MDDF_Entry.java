package starlight.taliis.core.chunks.adt;

import java.nio.ByteBuffer;
import starlight.taliis.core.memory;
/**
 * NOTE: scale and flags are wrong in wikis documentation.
 * Scale is at 20h and flags might be at 22h .. but was always 0
 * @author tharo
 *
 */
public final class MDDF_Entry extends memory {
	/*000h*/  public final static int nameId = 0;
	/*004h*/  public final static int uniqueId = 0x4;		
	/*008h*/  public final static int posY = 0x8; //float pos[3];		
	/*00Ch*/  public final static int posZ = 0xc;
	/*010h*/  public final static int posX = 0x10;
	/*014h*/  public final static int rotA = 0x14; //float rot[3];	
	/*018h*/  public final static int rotB = 0x18;
	/*01Ch*/  public final static int rotC = 0x1c;		
	/*020h*/  public final static int flags = 0x22;	
	/*022h*/  public final static int scale = 0x20;	// *1024
	/*024h*/  public final static int end = 0x24;
	
	public MDDF_Entry(ByteBuffer pointer) {
		super(pointer);
		buff.limit(end);
		
		pointer.position( pointer.position() + buff.limit() );
	}
	
	/**
	 * Creates a new DD apeareance. Scale will be set to 1024 (1:1)
	 * @param strIndex
	 * @param uID
	 * @param x
	 * @param y
	 * @param z
	 */
	public MDDF_Entry(int strIndex, int uID, float x, float y, float z) {
		super(end);
		
		setNameID(strIndex);
		setUniqID(uID);
		setX(x);
		setY(y);
		setZ(z);
		setScale((short)1024);
		buff.position(0);
	}
	
	/**
	 * Translates local to global coordinates and back 
	 * @param val coordinate u wish to translate
	 * @return counter value
	 */
	public static float translate(float val) {
		return 32*533.3333F - val;
	}
	
	public int getNameID() {
		return buff.getInt(nameId);
	}
	public void setNameID(int val) {
		buff.putInt(nameId, val);
	}
	
	public int getUniqID() {
		return buff.getInt(uniqueId);
	}
	public void setUniqID(int val) {
		buff.putInt(uniqueId, val);
	}
	
	public float getX() {	
		return buff.getFloat(posX);
	}
	public float getY() {
		return buff.getFloat(posY);
	}
	public float getZ() {
		return buff.getFloat(posZ);
	}
	public float getA() {
		return buff.getFloat(rotA);
	}
	public float getB() {
		return buff.getFloat(rotB);
	}
	public float getC() {
		return buff.getFloat(rotC);
	}
	public short getFlags() {
		return buff.getShort(flags);
	}
	public short getScale() {
		return buff.getShort(scale);
	}
	/**
	 * The "set" functions are just writes because the
	 * whole object allready got created somewere else 
	 * and we dont have to care about it.
	 */
	public void setX(float value) {
		buff.putFloat(posX, value);
	}
	public void setY(float value) {
		buff.putFloat(posY, value);
	}
	public void setZ(float value) {
		buff.putFloat(posZ, value);
	}
	public void setA(float value) {
		buff.putFloat(rotA, value);
	}
	public void setB(float value) {
		buff.putFloat(rotB, value);
	}
	public void setC(float value) {
		buff.putFloat(rotC, value);
	}
	public void setScale(short value) {	
		buff.putShort(scale, value);
	}
}