package starlight.taliis.core.chunks.adt;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import starlight.taliis.core.chunks.ChunkNotFoundException;
import starlight.taliis.core.chunks.SubChunkNotFoundException;
import starlight.taliis.core.chunks.chunk;
import starlight.taliis.core.files.adt;


/**
 * MCNK Fields beschreiben nahezu alles was den Boden betrifft und
 * haben in sich vereint zus�tzliche Chunks
 * 
 * complete redone. the mcnk now uses the offsets to load subchunks.
 * rendering is still done by logical structure
 * 
 * @author ganku
 *
 */

public final class MCNK extends chunk {
	// flags for format
	int version_flag = adt.ADT_VERSION_NORMAL;

	int size = 0;
	int rSize = 0;

	
	public static final float gfxWidth = 33.33333333333F;
	
	
	/*000h*/ public static final int flags = 0x0 +data;
	
	/*004h*/ public static final int IndexX = 0x4 +data;
	/*008h*/ public static final int IndexY = 0x8 +data;
	
	/*00Ch*/ public static final int nLayers = 0xC		+data;
	/*010h*/ public static final int nDoodadRefs = 0x10	+data;
	
	/*014h*/ public static final int offsHeight = 0x14	+data;
	/*018h*/ public static final int offsNormal = 0x18	+data;
	/*01Ch*/ public static final int offsLayer = 0x1C	+data;
	/*020h*/ public static final int offsRefs = 0x20	+data;
	
	/*024h*/ public static final int offsAlpha = 0x24	+data;	
	/*028h*/ public static final int sizeAlpha = 0x28	+data;
	/*02Ch*/ public static final int offsShadow = 0x2c	+data;
	/*030h*/ public static final int sizeShadow = 0x30	+data;
	
	/*034h*/ public static final int areaid = 0x34 +data;
	
	/*038h*/ public static final int nMapObjRefs = 0x38	+data;
	/*03Ch*/ public static final int holes = 0x3C		+data;
	
	/*040h*/ public static final int unk1 = 0x40 +data;
	/*042h*/ public static final int unk2 = 0x42 +data;
	/*044h*/ public static final int unk3 = 0x44 +data;
	/*048h*/ public static final int unk4 = 0x48 +data;
	/*04ch*/ public static final int unk5 = 0x4C +data;
	
	/*050h*/ public static final int predTex = 0x50 		+data;
	/*054h*/ public static final int noEffectDoodad = 0x54	+data;
	/*058h*/ public static final int offsSndEmitters = 0x58	+data;
	/*05ch*/ public static final int nSndEmitters = 0x5C	+data;

	/*060h*/ public static final int offsLiquid = 0x60		+data;
	/*064h*/ public static final int sizeLiquid = 0x64		+data;
	
	/*068h*/ public static final int posX = 0x68	+data;
	/*06ch*/ public static final int posY = 0x6C	+data; 
	/*070h*/ public static final int posZ = 0x70	+data;
	
	/*074h*/ public static final int offsColorValues = 0x74	+data;
	/*078h*/ public static final int props = 0x78			+data;
	/*07ch*/ public static final int effectId = 0x7C		+data;
	
	/*080h*/ public static final int end = 0x80	+data;
	
	// sub chunks
	public MCVT mcvt;
	public MCCV mccv;
	public MCNR mcnr;
	public MCLY mcly;
	public MCRF mcrf;
	public MCSH mcsh = null;
	public MCAL mcal;
	public MCLQ mclq = null;
	public MCSE mcse = null;
	
	/**
	 * Load the MCNK chunk structure providen at the current 
	 * position of a ByteBuffer.
	 * @param pointer
	 * @throws ChunkNotFoundException 
	 */
	public MCNK(ByteBuffer pointer) throws ChunkNotFoundException 
	{
		super(pointer, "KNCM");
		try {
			load(pointer);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			try {
				System.out.println("MCNK-offsets not valid!");
				parse(pointer);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		

	}
	/**
	 * Load the MCNK chunk structure providen at the current 
	 * position of a ByteBuffer.
	 * Also takes care of the ADT file version.
	 * @param pointer
	 * @param version 
	 * @throws ChunkNotFoundException 
	 */
	public MCNK(ByteBuffer pointer, int version) throws ChunkNotFoundException 

	{
		super(pointer, "KNCM");
		version_flag = version;
		try {
			load(pointer);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			try {
				System.out.println("MCNK-offsets not valid!");
				parse(pointer);
			} catch (SubChunkNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}

	
	private void load(ByteBuffer pointer) throws SubChunkNotFoundException {

		size = buff.getInt();

		// read sub-chunks
		try {
			buff.position(getHeightOffs());
			mcvt = new MCVT(buff);
			
			// color subchunk ?
			if(version_flag==adt.ADT_VERSION_EXPANSION_WOTLK) {
				if(getColorValueOffs()!=0) {
					buff.position(getColorValueOffs());
					if(chunk.nextChunk(buff).compareTo("VCCM")==0)
					mccv = new MCCV(buff);
				}
			}
			buff.position(getNormalsOffs());
			mcnr = new MCNR(buff);
			buff.position(getTexLayerOffs());
			mcly = new MCLY(buff);
		
			buff.position(getRefsOffs());
			mcrf = new MCRF(buff);
	
			// optional shadow!!
			if(getShadowOffs()!=getAlphaOffs()) {
				buff.position(getShadowOffs());
				mcsh = new MCSH(buff);
			}
			
			buff.position(getAlphaOffs());
			mcal = new MCAL(buff);
			
			// liquid subchunk?
			if(version_flag==adt.ADT_VERSION_NORMAL ||
			   version_flag==adt.ADT_VERSION_EXPANSION_TBC) {
				buff.position(getLiquitOffs());
				mclq = new MCLQ(buff);
			}
			
			// also optional ?
			if(getnSoundE()!=0){
			System.out.println("MCSE-offs: "+getSoundEOffs());
			buff.position(getSoundEOffs());
			mcse = new MCSE(buff);
			}
		} catch(ChunkNotFoundException e) {
			//TODO - try to catch the subchunks manually
				// maybe fix them?
			throw (SubChunkNotFoundException) e;
		}

		// limit our space
		buff.limit( buff.position() );
		pointer.position( pointer.position() + buff.position() );
	}
	
	
	private void parse(ByteBuffer pointer) throws SubChunkNotFoundException {
		buff.position(0);
		size = buff.getInt();

		// read sub-chunks
		try {

			mcvt = new MCVT(buff);
			
			// color subchunk ?
			if(version_flag==adt.ADT_VERSION_EXPANSION_WOTLK) {
				if(getColorValueOffs()!=0) {
					if(chunk.nextChunk(buff).compareTo("VCCM")==0)
					mccv = new MCCV(buff);
				}
			}
			mcnr = new MCNR(buff);
			mcly = new MCLY(buff);
		
			mcrf = new MCRF(buff);
	
			// optional shadow!!
			if(getShadowOffs()!=getAlphaOffs()) {
				mcsh = new MCSH(buff);
			}
			

			mcal = new MCAL(buff);
			
			// liquid subchunk?
			if(version_flag==adt.ADT_VERSION_NORMAL ||
			   version_flag==adt.ADT_VERSION_EXPANSION_TBC) {
				mclq = new MCLQ(buff);
			}
			
			// also optional ?
			if(chunk.nextChunk(buff).compareTo("ESCM")==0)
			mcse = new MCSE(buff);
		} catch(ChunkNotFoundException e) {
			//TODO - try to catch the subchunks manually
				// maybe fix them?
			throw (SubChunkNotFoundException) e;
		}

		// limit our space
		buff.limit( buff.position() );
		pointer.position( pointer.position() + buff.position() );
	}
	
	/**
	 * Creates a new MCNK Field at that is meant to be located
	 * at the given x and y index inside of the adt file.
	 * @param x
	 * @param y
	 */
	public MCNK(int x, int y) {
		super(end);
		create(x,y);
	}
	/**
	 * Creates a new MCNK Field at that is meant to be located
	 * at the given x and y index inside of the adt file.
	 * The version of the adt file is also providen.
	 * @param x
	 * @param y
	 * @param version
	 */	
	public MCNK(int x, int y, int version) {
		super(end);
		version_flag = version;
		create(x,y);
	}
	
	private void create(int x, int y) {
		byte magic[] = { 'K', 'N', 'C', 'M'};
		
		buff.put(magic);		// magic
		buff.putInt(end - 8);	// size
		
		// set some default values ..
		buff.putInt(IndexX, x);
		buff.putInt(IndexY, y);
		
		if(DEBUG) System.out.println("Empty MCNK created.");
		
		// create subchunks		
		mcvt = new MCVT();
		// color subchunk?
		if(version_flag==adt.ADT_VERSION_EXPANSION_WOTLK)
			mccv = new MCCV();
		mcnr = new MCNR();
		mcly = new MCLY();
		mcrf = new MCRF();
		if(version_flag<adt.ADT_VERSION_EXPANSION_WOTLK)
		mcsh = new MCSH();		// always providen
		mcal = new MCAL();
		// liquid subchunk?
		if(version_flag==adt.ADT_VERSION_NORMAL || version_flag==adt.ADT_VERSION_EXPANSION_TBC)
		{	
			mclq = new MCLQ();
			setLiquitSize(8*8+(9*9*8)+0x54+8);
		}
		mcse = new MCSE();
		
		// reset position
		buff.position(0);	
	}
		
	/**
	 * re-calculate my own size ..
	 * 	and btw also all of my inner chunks offsets ..
	 */
	public void render() {
		int ro = end;
		
		setHeightOffs(ro);
		mcvt.render();
		ro+=mcvt.getSize();
		
		// color values?
		if(mccv!=null) {
			setColorValueOffs(ro);
			mccv.render();
			ro+=mccv.getSize();
		}

		setNormalsOffs(ro);
		mcnr.render();
		ro+=mcnr.getSize();
		
		setTexLayerOffs(ro);
		mcly.render();
		ro+=mcly.getSize();
		
		setRefsOffs(ro);
		mcrf.render();
		ro+=mcrf.getSize();
		
		setShadowOffs(ro);
		// shadows?
		if(mcsh!=null)  {
			mcsh.render();
			ro+=mcsh.getSize();
			
			// the size field ALWAYS exclude the chunk header
			setShadowSize(mcsh.getSize() -data/**/);
		}
		
		setAlphaOffs(ro);
		mcal.render();
		ro+=mcal.getSize();
		setAlphaSize(mcal.getSize());

		// liquid?
		if(mclq!=null) {
			setLiquitOffs(ro);
			mclq.render();
			if(mclq.isempty!=true){
			setLiquitSize(8*8+(9*9*8)+0x54+8+8);
			ro+=8*8+(9*9*8)+0x54+8+8;
			setFlags(5);
			}
			else{
				setLiquitSize(8);
				ro+=8;
				setFlags(1);				
			}
		}
		if(mcse!=null){
		setSoundEOffs(ro);
		mcse.render();
		ro+=mcse.getSize();
		}
		
		buff.putInt(chunkSize, ro);
		buff.position(0);
		
		rSize = ro;
		
	// write back to memory
		// new memory of course otherwise we wont be able
		// to render more then once
		ByteBuffer tmp = ByteBuffer.allocate(ro);
		tmp.order(ByteOrder.LITTLE_ENDIAN);
		tmp.position(0);

		// copy the offsets and stuff
		for(int c=0; c<end; c++)
			tmp.put(buff.get());
		//tmp.put(buff);		// copy my offsets and stuff
		tmp.position(end);	// overwrite everything after the header-data
		
		
		tmp.put( mcvt.buff );
		if(mccv!=null) tmp.put( mccv.buff );
		tmp.put( mcnr.buff );
		tmp.put( mcly.buff );
		tmp.put( mcrf.buff );
		if(mcsh!=null) tmp.put( mcsh.buff );
		tmp.put( mcal.buff );
		if(mclq!=null) tmp.put( mclq.buff );
		if(mcse!=null)tmp.put( mcse.buff );
		
		buff = tmp;
		writeSize();
		buff.position(0);
	}
	
	
	
	/**
	 * @return Offset where normal vertexes chunk starts
	 */
	public int getNormalsOffs() {
		return buff.getInt(offsNormal);
	}
	public void setNormalsOffs(int value) {
		buff.putInt(offsNormal, value);
	}
	
	/**
	 * @return Offset where height values chunk starts
	 */
	public int getHeightOffs() {
		return buff.getInt(offsHeight);
	}
	public void setHeightOffs(int value) {
		buff.putInt(offsHeight, value);
	}
	
	
	/**
	 * @return Offset were texture layer chunk starts
	 */
	public int getTexLayerOffs() {
		return buff.getInt(offsLayer);
	}
	public void setTexLayerOffs(int value) {
		buff.putInt(offsLayer, value);
	}
	public int getRefsOffs() {
		return buff.getInt(offsRefs);
	}
	public void setRefsOffs(int value) {
		buff.putInt(offsRefs, value);
	}
	public int getShadowOffs() {
		return buff.getInt(offsShadow );
	}
	public void setShadowOffs(int value) {
		buff.putInt(offsShadow , value);
	}
	public int getAlphaOffs() {
		return buff.getInt(offsAlpha);
	}
	public void setAlphaOffs(int value) {
		buff.putInt(offsAlpha, value);
	}
	public int getLiquitOffs() {
		return buff.getInt(offsLiquid);
	}
	public void setLiquitOffs(int value) {
		buff.putInt(offsLiquid, value);
	}
	public int getSoundEOffs() {
		return buff.getInt(offsSndEmitters);
	}
	public void setSoundEOffs(int value) {
		buff.putInt(offsSndEmitters, value);
	}
	
	public int getnSoundE(){
		return buff.getInt(nSndEmitters);
	}
	public void setnSoundE(int val){
		buff.putInt(nSndEmitters, val);
	}
	
	
	public int getIndexX() {
		return buff.getInt(IndexX);
	}
	public void setIndexX(int val) {
		buff.putInt(IndexX, val);
	}
	public int getIndexY() {
		return buff.getInt(IndexY);
	}
	public void setIndexY(int val) {
		buff.putInt(IndexY, val);
	}
	public float getPosX() {
		return buff.getFloat(posX);
	}
	public float getPosY() {
		return buff.getFloat(posY);
	}
	public float getPosZ() {
		return buff.getFloat(posZ);
	}
	public void setPosX(float val) {
		buff.putFloat(posX, val);
	}
	public void setPosY(float val) {
		buff.putFloat(posY, val);
	}
	public void setPosZ(float val) {
		buff.putFloat(posZ, val);
	}		
	public int getNLayers() {
		return buff.getInt(nLayers);
	}
	public void setNLayers(int val) {
		buff.putInt(nLayers, val);
	}
	public int getNDoodad() {
		return buff.getInt(nDoodadRefs);
	}
	public void setNDoodad(int val) {
		buff.putInt(nDoodadRefs, val);
	}
	public int getTextureID() {
		return buff.getInt(offsColorValues);
	}
	public void setTextureID(int val) {
		buff.putInt(offsColorValues, val);
	}
	public int getColorValueOffs() {
		return buff.getInt(offsColorValues);
	}
	public void setColorValueOffs(int val) {
		buff.putInt(offsColorValues, val);
	}
	
	
	//TODO: WHAT is that ?
	public int getPredTex() {
		return buff.getInt(predTex);
	}
	public int getAreaID() {
		return buff.getInt(areaid);
	}
	public void setAreaID(int val) {
		buff.putInt(areaid, val);
	}
	
	public int getHoles() {
		return buff.getInt(holes);
	}
	public void setHoles(int val) {
		buff.putInt(holes, val);
	}
	
	public int getFlags() {
		return buff.getInt(flags);
	}
	public void setFlags(int val) {
		buff.putInt(flags, val);
	}
	
	public void setNObj(int val) {
		buff.putInt(nMapObjRefs, val);
	}
	public int getNObj() {
		return buff.getInt(nMapObjRefs);
	}
	
	public void setAlphaSize(int val) {
		buff.putInt(sizeAlpha, val);
	}
	public int getAlphaSize() {
		return buff.getInt(sizeAlpha);
	}
		 	
	public void setLiquitSize(int val) {
		buff.putInt(sizeLiquid, val);
	}
	public int getLiquitSize() {
		return buff.getInt(sizeLiquid);
	}
	
	public void setShadowSize(int val) {
		buff.putInt(sizeShadow, val);
	}
	public int getShadowSize() {
		return buff.getInt(sizeShadow);
	}
	
	
	/**
	 * Creates a complete new layer including 
	 * 	layer, shadow and alpha.
	 * @param texture_id that get displayed then
	 * @return id of our new layer or neg. if error occured
	 */
	public int createLayer(int texture_id, int area_id) {
		int layers = getNLayers(); 
		if(layers==4) return -1;
		
		// create shadow (is allready created)
		
		// create layer
		mcly.add(texture_id,0);
		setNLayers(layers+1);
		
		// set my flags to default
		setFlags(0x1);
				
		// set area id
		setAreaID(area_id);
		
		return mcly.getLength();
	}
	
	
	/**
	 * Gives back our size including all datas in byte
	 * get set by render()
	 * 
	 * @return
	 */
	public int getRenderedSize() {
		return rSize;
	}

	/**
	 * The ADT file subversion that is used
	 * @return
	 */
	public int getVersion(){
		return version_flag;
	}
}

class MCNK_Flag {
	public static final int FLAG_SHADOW = 1;
	public static final int FLAG_IMPASS = 2;
	public static final int FLAG_LQ_RIVER = 3;
	public static final int FLAG_LQ_OCEAN = 4;
	public static final int FLAG_LQ_MAGMA = 5;
}
