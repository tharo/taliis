package starlight.taliis.core.chunks.adt;

import java.nio.ByteBuffer;
import starlight.taliis.core.memory;

public final class MH2O_Entry extends memory {
	/*0x00*/	public final static int offsData1 = 0x0;
	/*0x04*/	public final static int used = 0x4;
	/*0x08*/	public final static int offsData2 = 0x8;
	/*0x0C*/	public final static int end = 0xC;
	
	public MH2O_Entry(ByteBuffer pointer) {
		super(pointer);
		buff.limit(end);
		pointer.position( pointer.position() + buff.limit() );
	}
	
	public int GetoffsData1(){
		return buff.getInt(offsData1);
	}
	
	public void SetOffsData1(int val){
		buff.putInt(offsData1, val);
	}
	
	public int Getused(){
		return buff.getInt(used);
	}
	
	public void Setused(int val){
		buff.putInt(used, val);
	}
	
	public int GetoffsData2(){
		return buff.getInt(offsData2);
	}
	
	public void SetOffsData2(int val){
		buff.putInt(offsData2, val);
	}
}