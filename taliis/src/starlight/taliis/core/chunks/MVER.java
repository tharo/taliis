package starlight.taliis.core.chunks;

import java.nio.ByteBuffer;

/**
 * Blizzard's MVER chunk that is used in nearly every file first and on top
 */
public final class MVER extends chunk {
	/**
	 * Create a new MVER chunk with version 1.2
	 */
	public MVER() {
		super(0xC);
		byte magic[] = { 'R', 'E', 'V', 'M' };

		buff.put(magic);
		buff.putInt(4);
		buff.putInt(0x12);

		// reset position
		buff.position(0);

		if (DEBUG)
			System.out.println("MVER created.");
	}

	/**
	 * Read a MVER chunk from a pointer.
	 * 
	 * @param pointer A ByteBuffer of the chunk.
	 * @throws ChunkNotFoundException Thrown if the chunk is not found.
	 */
	public MVER(ByteBuffer pointer) throws ChunkNotFoundException {
		super(pointer, "REVM");

		// read in main chunks (size)
		int s = buff.getInt(chunkSize);
		buff.limit(data + s);

		// push pointer
		pointer.position(data + s);
	}

	/**
	 * Create a new MVER chunk with a given version.
	 * 
	 * @param version
	 */
	public MVER(int version) {
		super(0xC);
		byte magic[] = { 'R', 'E', 'V', 'M' };

		buff.put(magic);
		buff.putInt(4);
		buff.putInt(version);

		// reset position
		buff.position(0);

		if (DEBUG)
			System.out.println("MVER created.");
	}

	/**
	 * @return Version of the File Type
	 */
	public int getVersion() {
		return buff.getInt(data);
	}

	/**
	 * Set the new version
	 * 
	 * @param version
	 */
	public void setVersion(int version) {
		buff.putInt(data, version);
	}
}
