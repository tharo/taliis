package starlight.taliis.core.chunks.wdt;

/**
 * Map tile table. 
 * contains 64x64 = 4096 records of 8 bytes each.
 * Each record can be considered a 64-bit integer: 
 * 	1 if a tile is present, 0 otherwise.
 */

import java.nio.*;

import starlight.taliis.core.chunks.ChunkNotFoundException;
import starlight.taliis.core.chunks.chunk;


public final class MAIN extends chunk {	
	/**
	 * Init our datas by given Pointer
	 * @param pointer
	 */
	public MAIN(ByteBuffer pointer) throws ChunkNotFoundException {
		super(pointer, "NIAM");
		
		int size = buff.getInt();
		
		buff.limit( size + data );
		pointer.position( pointer.position() + buff.limit() ); 
	}
	
	/**
	 * Creates new chunks
	 */
	public MAIN() {
		super(64*64*8 + data);
		byte magic[] = { 'N', 'I', 'A', 'M'};
		
		buff.put(magic);		// magic
		buff.putInt(64*64*8);	// size
		
		buff.position(0);
		if(DEBUG) System.out.println("MPHD created.");
	}
	
	/**
	 * Returns the value of given field
	 * @param x
	 * @param y
	 * @return the value
	 */
	public long getValue(int x, int y) {
		int index = (y*64) + x;
		return buff.getLong(index*8 + data);
	}
	/**
	 * Set the values of given field
	 * @param x
	 * @param y
	 * @param val
	 */
	public void setValue(int x, int y, long val) {
		int index = (y*64) + x;
		buff.putLong(index*8 + data, val);
	}
}
