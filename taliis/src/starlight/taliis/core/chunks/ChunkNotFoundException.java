package starlight.taliis.core.chunks;

/**
 * This exception is created to be thrown if a chunk could - for some reason -
 * not be found.
 * 
 * @author ganku
 * 
 */
@SuppressWarnings("serial")
public class ChunkNotFoundException extends Exception {
	String magic = null;

	public ChunkNotFoundException() {
	}

	/**
	 * Creates a new Exception.
	 * 
	 * @param s A message.
	 * @param magic The chunk's identifier.
	 */
	ChunkNotFoundException(String s, String magic) {
		super(s);
		this.magic = magic;
	}

	/**
	 * Returns the chunk's magic id.
	 * 
	 * @return The identifer which is set when throwing the exception.
	 */
	public String getMagic() {
		return magic;
	}
}
