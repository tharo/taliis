package de.taliis.editor;

import java.io.File;
import java.io.IOException;
import java.io.InvalidClassException;
import java.util.Vector;

import starlight.taliis.core.chunks.ChunkNotFoundException;
import starlight.taliis.core.files.wowfile;

import de.taliis.editor.plugin.Plugin;
import de.taliis.editor.plugin.PluginStorage;
/**
 * The File Mananger is designed to 
 *  - open
 *  - init
 *  - save
 *  
 *  Given data classes.
 *  Also its designed to load corespodending
 *  files such as textures and stuff.
 * 
 * @author Administrator
 *
 */
public class fileMananger {
	private Vector<Plugin> ppool;
	private Vector<openedFile> files;
	private openedFile activeFile = null;
	/**
	 * @param pp reference to main applications plugin pool
	 */
	public fileMananger(Vector<Plugin> pp) {
		ppool = pp;
		files = new Vector<openedFile>();
	}
	/**
	 * @return list of loaded files
	 */
	public Vector<openedFile> getFileList() {
		return files;
	}
	public String[] getFileNames() {
		String[] tmp = new String[files.size()];
		int c = 0;
		for(openedFile f : files)
			tmp[c++] = f.f.getName();
		return tmp;
	}
	
	/**
	 * open the given file location
	 * @param path
	 * @return
	 * @throws ChunkNotFoundException 
	 * @throws InvalidClassException 
	 */
	public openedFile openFileLocation(String path) throws InvalidClassException, ChunkNotFoundException {
	// try to open the file
		File tmp = new File(path);
		if(	tmp.exists() == false ||
			tmp.canRead() == false
		   ) {
			System.err.println("Cannot read File " + path + ".");
			return null;
		}
		return openFile(tmp);
	}
	
	/**
	 * open the given file 
	 * @param f file object
	 * @return
	 * @throws ChunkNotFoundException 
	 * @throws InvalidClassException 
	 */
	public openedFile openFile(File f) throws InvalidClassException, ChunkNotFoundException {
		try {
			f = f.getCanonicalFile();
		} catch(IOException e) {};
		
		openedFile of = new openedFile(f);

	// is this file allready opened??
		for(openedFile tmp : files) {
			
			// it is. Let user touch it.
			if(tmp.f.compareTo(f)==0) {
				tmp.setFileOpener(tmp.OPENED_BY_USER);
				return tmp;
			}
		}
		
	// find a plugin that can handle this file	
		String name = f.getName();
		Plugin storage = null;
		
		for(Plugin p : ppool) {
			if((p.getPluginType() & p.PLUGIN_TYPE_STORAGE)!=0) {
				for(String s : p.getSupportedDataTypes()) {
					if(name.toLowerCase().endsWith(s)) {
						storage = p;
					}
				}
			}
		}
		if(storage==null) {
			System.err.println("Cannot open " + f.getName() + ".");
			System.err.println("File extension is not supported.");
			return null;
		}
		else {
			PluginStorage ps = (PluginStorage)storage;
			of.obj = ps.load(f);
		}
		
		files.add(of);
		return of;
	}
	
	public int registerObject(wowfile o, File f) {
		openedFile of = new openedFile(o, f);
		files.add(of);
		return 1;
	}
	
	public int saveFile(openedFile f) {
		if(f.f==null) {
			System.err.println("File got not prepared to have a file name!");
			return -1;
		}
		
		String name = f.f.getName();
		Plugin storage = null;
		
		for(Plugin p : ppool) {
			if((p.getPluginType() & p.PLUGIN_TYPE_STORAGE)!=0) {
				for(String s : p.getSupportedDataTypes()) {
					if(name.endsWith(s)) {
						storage = p;
					}
				}
			}
		}
		
		if(storage==null) return -1;
		PluginStorage s = (PluginStorage) storage;
		
		s.save(f);
		
		return 0;
	}

	/**
	 * closes the given reverence and kill it
	 * out of the file list
	 * 
	 * @param f
	 * @return
	 */
	public int closeFile(openedFile f) {
		files.remove(f);
		f = null;
		activeFile = null;
		return 1;
	}

	/**
	 * Get the opened file by an linear index
	 * @param index
	 * @return
	 */
	public openedFile getFile(int index) {
		//NOTE maybe this wont work after closing some internal
		// files. Maybe count by hand in a for each loop ...
		return files.elementAt(index);
	}
	
	/**
	 * Set a file as "choosen" .. needed for other
	 * parts of the application
	 * @param active
	 */
	public void setActiveFile(openedFile active) {
		activeFile = active;
	}
	/**
	 * @return the active marked file
	 */
	public openedFile getActiveFile() {
		return activeFile;
	}
}