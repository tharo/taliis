package de.taliis.editor.plugin;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Properties;
import java.util.Vector;


/**
 * A Plugin pack is a collection of some plugins and its
 * corespodending classes inside of a JAR archive.
 * 
 * A plugin pack loads all included plugins in a valid JAR
 * archive. Therefore the "plugins.txt" have to be in the
 * root directory of this plugin pack file.
 * 
 * @author Tharo Herberg
 */

public class PluginPack {
	public Vector<Plugin> Plugins = null;
	Properties config;
	URLClassLoader classLoader;
	File archive;
	
	/**
	 * Loads/init the given plugin file 
	 * @param PluginArchive
	 */
	public PluginPack(File PluginArchive){
		archive = PluginArchive;
		load();
	}
	
	/**
	 * Loads the given JAR archive, trys to read out the plugins.txt
	 * and load all registered classes.
	 */
	private void load() {
		// load jar file into our classloader
		try {
			classLoader = new URLClassLoader(
							new URL[] {archive.toURI().toURL()}
						  );			
		} catch (MalformedURLException e) {
			//e.printStackTrace();
			return;
		}
		if(classLoader==null) return;
		
		// catch the config file
		InputStream resourceAsStream = classLoader.getResourceAsStream("plugins.txt");

		// read config file
		try {
			//propInFile = new FileInputStream( configFile );
			config = new Properties();
			config.load( resourceAsStream );
			resourceAsStream.close();
		} catch (IOException e) {
			//e.printStackTrace();
			return;
		} catch (NullPointerException en) {
			return;
		}
		
		// check for needed values
		String count = config.getProperty("plugin_count");
		if(count==null) {
			System.err.println("Invalid plugin config file.");
			return;
		}
		int numbers = new Integer(count);
		Plugins = new Vector<Plugin>();
		
		// extract all plugin classes to our vector
		for(int c=1; c<=numbers; c++) {
			String name = config.getProperty("plugin_" + c);
			if(name==null) return;
			System.out.println(" + " + name);
			// Load the class that was specified
	        Class<?> cls;
			try {
				cls = classLoader.loadClass(name);
			} catch (ClassNotFoundException e) {
				//e.printStackTrace();
				return;
			}

	        if (!Plugin.class.isAssignableFrom(cls)) {
	            // Lets just throw some exception since this class doesn't implement the Plugin interface
	            throw new RuntimeException("Invalid Pugin class");
	        }

	        // Create a new instance of the new class and return it
	        // We can be sure this wont throw a class cast exception since we checked for that earlier
	        try {
	        	// ad to our plugin collection
				Plugins.add( (Plugin)cls.newInstance() );
			} catch (InstantiationException e) {
				//e.printStackTrace();
			} catch (IllegalAccessException e) {
				//e.printStackTrace();
			}
		}
	}
	
	/**
	 * Reloads the archive 
	 */
	public void reload() {
		unload();
		load();
	}
	
	/**
	 * Unloads all ressources
	 */
	public void unload() {
		Plugins.clear();
		config = null;
		classLoader = null;
	}
	
	/**
	 * Returns the used Class Loader including
	 * all loaded resources.
	 * @return
	 */
	public URLClassLoader getLoader() {
		return classLoader;
	}
}
