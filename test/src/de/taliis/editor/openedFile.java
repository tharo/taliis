package de.taliis.editor;

import java.io.File;

import starlight.taliis.core.files.wowfile;

/**
 * Just a stupid storage class
 */

public class openedFile {
	public File f;
	public Object obj;
	private String ext;
	
	public openedFile (File file) {
		f = file;
		obj = null;
		
		int tmp = f.getName().lastIndexOf(".");
		ext = f.getName().substring(tmp+1);
	}
	public openedFile(Object o) {
		obj = o;
		f = null;
		
		int tmp = o.getClass().toString().lastIndexOf(".");
		ext = f.getName().substring(tmp+1);
	}
	
	public String toString() {
		return f.getName();
	}
	public String getExtension() {
		return ext;
	}
}