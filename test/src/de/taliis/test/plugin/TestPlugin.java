package de.taliis.test.plugin;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.filechooser.FileFilter;

import starlight.taliis.core.files.adt;
import starlight.taliis.core.files.wowfile;
import starlight.taliis.helpers.fileLoader;

import de.taliis.editor.configMananger;
import de.taliis.editor.fileMananger;
import de.taliis.editor.openedFile;
import de.taliis.editor.plugin.*;

/**
 * This is a smal plugin that was made to demonstrate 
 * some basic things.
 * 
 * - How to create some menue entrys
 * - How to load some icons from plugins own 
 * - Shows how to deal with the fileMananger
 * - use the global config file
 * 
 * @author Tharo Herberg
 *
 */
public class TestPlugin 
	implements Plugin, ActionListener 
{
	ImageIcon iNew, iOpen, iClose, iSave, iSaveAs;
	JMenuItem mNew, mOpen, mClose, mSave, mSaveAs;
	
	Vector<Plugin>ppool;
	fileMananger fm;
	eventServer es;
	configMananger cm;
	
	String myConfigStr = "taliis_SwingSysDialog_";
	
	
	public int getPluginType() {
		return this.PLUGIN_TYPE_FUNCTION;
	}

	
	public void setClassLoaderRef(URLClassLoader ref) {
		URL u;
		try {
			u = ref.getResource("icons/page.png");
			iNew = new ImageIcon( u );
			
			u = ref.getResource("icons/folder_go.png");
			iOpen = new ImageIcon( u );
			
			u = ref.getResource("icons/folder.png");
			iClose = new ImageIcon( u );
			
			u = ref.getResource("icons/disk.png");
			iSave = new ImageIcon( u );
			
			u = ref.getResource("icons/disk_multiple.png");
			iSaveAs = new ImageIcon( u );
		} catch(NullPointerException e) {}
	}

	
	public void setFileManangerRef(fileMananger ref) {
		// TODO Auto-generated method stub
		fm = ref;
	}
	
	
	public void setEventServer(eventServer ref) {
		es = ref;
	}
	
	 
	public void setConfigManangerRef(configMananger ref) {
		cm = ref;
	}
	
	
	
	public void setMenuRef(JMenuBar ref) {
		// TODO Auto-generated method stub
		mNew = new JMenuItem("New ..");
		mNew.addActionListener(this);
		mNew.setIcon(iNew);
		
		mOpen = new JMenuItem("Open");
		mOpen.addActionListener(this);
		mOpen.setIcon(iOpen);
		
		mClose = new JMenuItem("Close");
		mClose.addActionListener(this);
		mClose.setIcon(iClose);
		
		mSave = new JMenuItem("Save");
		mSave.addActionListener(this);
		mSave.setIcon(iSave);
		
		mSaveAs = new JMenuItem("Save As..");
		mSaveAs.addActionListener(this);
		mSaveAs.setIcon(iSaveAs);
		
		JMenu fm = ref.getMenu(0);
		fm.insertSeparator(0);
		fm.add(mSaveAs, 0);
		fm.add(mSave, 0);
		fm.insertSeparator(0);
		fm.insert(mClose, 0);
		fm.add(mOpen, 0);
		fm.insertSeparator(0);
		fm.add(mNew, 0);
	}

	 
	public void setPluginPool(Vector<Plugin> ref) { 
		ppool = ref;
	}
	
	
	public void actionPerformed(ActionEvent e) {
		//TODO: handle last opened File
		
		if(e.getSource()==mOpen) {
			JFileChooser fc = new JFileChooser();
			
			// last opened dir?
			String dir = cm.getGlobalConfig().getProperty(
					myConfigStr + "lastdir"
				);
			if(dir!=null)
				fc.setCurrentDirectory(new File(dir));
	
			// filter?
			for(Plugin p : ppool) {
				if(p instanceof PluginStorage) {
					FileFilter tmp = ((PluginStorage)p).getFiter();
					if(tmp!=null) fc.addChoosableFileFilter( tmp );
				}
			}

			fc.showOpenDialog(null);
			
			if(fc.getSelectedFile()==null) return;
			fm.openFile( fc.getSelectedFile() );
			es.updateTable();
			
			// save latest dir
			cm.getGlobalConfig().setProperty(
					myConfigStr + "lastdir", 
					fc.getSelectedFile().getAbsolutePath()
				);
		}
		else if(e.getSource()==mClose) {
			if(fm.getActiveFile()==null) return;
			
			fm.closeFile( fm.getActiveFile() );
			es.updateTable();
		}
		else if(e.getSource()==mSave) {
			if(fm.getActiveFile()==null) return;
			
			fm.saveFile( fm.getActiveFile() );
		}
		else if(e.getSource()==mSaveAs) {
			if(fm.getActiveFile()==null) return;
			
			JFileChooser fc = new JFileChooser();
			openedFile s = fm.getActiveFile();
			fc.setCurrentDirectory( s.f );
			fc.showSaveDialog( null );
			
			if(fc.getSelectedFile()==null) return;
			s.f = fc.getSelectedFile();
			
			fm.saveFile( s );
			es.updateTable();
			
			// save latest dir
			cm.getGlobalConfig().setProperty(
					myConfigStr + "lastdir", 
					s.f.getAbsolutePath()
				);
		}
	}

	 public ImageIcon getIcon() { return null; }
	 public boolean checkDependencies() { return true; }
	 public String[] getSupportedDataTypes() {	return new String[]{ }; }
	 public String[] neededDependencies() { return null; }
}