package de.taliis.plugins.wowmpq;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.filechooser.FileFilter;

import starlight.taliis.core.files.wowfile;

import de.taliis.dialogs.mpqSources;
import de.taliis.editor.configMananger;
import de.taliis.editor.fileMananger;
import de.taliis.editor.openedFile;
import de.taliis.editor.plugin.PluginStorage;
import de.taliis.editor.plugin.Plugin;
import de.taliis.editor.plugin.eventServer;


public class mpqStorage implements Plugin, PluginStorage, ActionListener {
	ImageIcon icon = null;
	configMananger cm;
	JMenuItem mSources;
	Vector<Plugin> plugins = null;
	
	public boolean checkDependencies() { return true; }
	public ImageIcon getIcon() { return icon; }

	public int getPluginType() {
		return this.PLUGIN_TYPE_STORAGE;
	}

	public String[] getSupportedDataTypes() {
		return new String[]{ "mpq" }; 
	}

	public String[] neededDependencies() {
		return null;
	}

	public void setClassLoaderRef(URLClassLoader ref) { 
		try {
			URL u = ref.getResource("icons/book.png");
			icon = new ImageIcon( u );
		} catch(NullPointerException e) {}		
	}

	public void setConfigManangerRef(configMananger cm) {
		this.cm = cm;
		
		// first start?
		if(cm.getGlobalConfig().getProperty("taliis_mpqLoader_init")==null)
			new mpqSources(cm);
	}

	public void setEventServer(eventServer arg0) { }

	public void setFileManangerRef(fileMananger arg0) {	}

	public void setPluginPool(Vector<Plugin> arg0) { 
		plugins = arg0;
	}
	
	
	/**
	 * Menue entry for the sources dialog
	 */
	public void setMenuRef(JMenuBar ref) {	
		mSources = new JMenuItem("MPQ sources ...");
		 mSources.addActionListener(this);
		 mSources.setIcon(icon);
		 
		// get our sub menue
		for(int c=0; c<ref.getMenuCount(); c++) {
			JMenu menu = ref.getMenu(c);
			if(menu.getName().compareTo("menu_settings")==0) {
				menu.setEnabled(true);
				menu.add(mSources, 0);
				return;
			}
		}
	}
	
	
	public wowfile create() {
		System.err.println("Theres no need for a create function.");
		return null;
	}
	
	public FileFilter getFiter() {
		return new FileFilter() {

			public boolean accept(File arg0) {
				if(arg0.isDirectory())return true;
				if(arg0.toString().toLowerCase().endsWith(".mpq")) return true;
				else return false;
			}

			public String getDescription() {
				return "WOW MPQ Archives";
			}
		};
	}
	
	public wowfile load(File arg0) {
		return new mpq(arg0);
	}
	
	public int save(openedFile arg0) {
		// no savings
		return -1;
	}
	public void unload() {
		// TODO Auto-generated method stub
	}
	
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==mSources) {
			new mpqSources(cm);
			
			// reload mpq loaders list
			if(plugins==null) return;
			for(Plugin p : plugins) {
				if(p.getClass().toString().contains("mpqFileRequest")) {
					((mpqFileRequest)p).reloadArchiveList();
					break;
				}
			}
		}
	}
}
