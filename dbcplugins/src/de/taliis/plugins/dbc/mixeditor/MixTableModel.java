package de.taliis.plugins.dbc.mixeditor;

import javax.swing.table.AbstractTableModel;

import starlight.taliis.core.ZeroTerminatedString;
import starlight.taliis.core.files.dbc;

public class MixTableModel extends AbstractTableModel {
	MixDataSet data;
	int types[];
	
	public MixTableModel(MixDataSet data) {
		this.data = data;
		this.types = data.types;
	}

	public int getColumnCount() {
		return data.dbcFile.getNFields();
	}

	public int getRowCount() {
		return data.dbcFile.getNRecords();
	}

    public String getColumnName(int col) {
    	if(data.configRevIndex!=-1) {
    		String ret = data.config.getProperty(
    				"dbc_entry_"+data.configRevIndex+"_field_"+col+"_label");
    		
    		if(ret!=null) return ret;
    	}
    	
    	return "#" + col;
    }
	
    
    // Funny .. this function just caused problems o.O
	public Class getColumnClass(int c) {     
    	if(types[c]==dbc.COL_TYPE_STRING) return ZeroTerminatedString.class;
    	//else if(types[c]==dbc.COL_TYPE_FLOAT) return float.class;
    	else if(types[c]==dbc.COL_TYPE_NUMERIC) return Integer.class;
    	else if(types[c]==dbc.COL_TYPE_BOOLEAN) return Boolean.class;
    	else return Object.class;
    }
	
    public Object getValueAt(int row, int col) {
    	Object t = data.dbcFile.getData(col, row);
    	int tv = ((Number)t).intValue();
    	
    	if(types[col]==dbc.COL_TYPE_STRING) {
    		ZeroTerminatedString tmp = data.dbcFile.getStringByOffset(tv);
    		if(tmp==null) {
    			System.err.println("ERROR! String " + tv + " not found! " + col);
    			return "";
    		}
    		return tmp;
    	}
    	else if(types[col]==dbc.COL_TYPE_FLOAT) {
    		//TODO: this makes the values HORROBLE rounded! find a better way!
    		float ret = Float.intBitsToFloat(tv);
    		return ""+ret;// Double.longBitsToDouble(t);
    	}
    	//Boolean field
    	else if(types[col]==dbc.COL_TYPE_BOOLEAN){
    		boolean ret = false;
    		if(tv==1)ret = true;
    		return ret;
    	}
    	else if(data.crossed[col]==true) {
    		int index = data.mixArrIndexies[col];
    		
    		// no crosslist ?
    		if(data.CrossLists == null || data.CrossLists[index]==null) {
    			return tv;
    		}
    		
    		// normal values?
    		if(types[col]==dbc.COL_TYPE_NUMERIC) {
    			if(data.CrossLists[index].get(tv)==null)
    				return new MixEntry(""+tv, tv);
    			
    			MixEntry tmp = new MixEntry(
    					data.CrossLists[index].get(tv),
    					tv
    					);
    			return tmp;
    		}
    		// masked type
    		if(types[col]==MixDataSet.COL_TYPE_MASK) {
    			int mask = 1;
    			String res = "";
    			
    			int fieldsize = data.dbcFile.getRecordSize() 
    								/ data.dbcFile.getNFields()
    								* 8;
    			for(int i=1; i<=fieldsize; i++) {
    				if((tv&mask)!=0) {
	    				String tmp = data.CrossLists[index].get(i);
	    				if(tmp!=null) {
	    					if(res.length()!=0) res = res + "; ";
	    					res = res + tmp;
	    				}
    				}
    				
    				mask <<= 1;
    			}
    			if(res.length()==0) res = "" + tv;
    			return new MixEntry(res, tv);
    		}
    	}
    	
    	return t;
    }
    
    public boolean isCellEditable(int row, int col) {
    	if(types[col]==dbc.COL_TYPE_STRING) return true;
    	else if(types[col]==dbc.COL_TYPE_FLOAT) return true;
    	else if(types[col]==dbc.COL_TYPE_NUMERIC) return true;
    	else if(types[col]==dbc.COL_TYPE_BOOLEAN) return true;
    	else if(data.crossed[col]==true) return true;
    	else return false;
    }
    
    public void setValueAt(Object value, int row, int col) {
    	if(types[col]==dbc.COL_TYPE_STRING) {
    		int index = Integer.valueOf((String)value) ;
    		if(data.dbcFile.getStringByOffset(index)!=null)
    			data.dbcFile.setData(col, row, index);
    		else System.err.println("Invalid String index.");/**/
    	}
    	else if(types[col]==dbc.COL_TYPE_FLOAT) {
    		int tmpi;
    		float tmpf;
    		// this multi transforming is SO BULLSHIT!
    		tmpf = Float.valueOf(value.toString());
    		tmpi = Float.floatToIntBits(tmpf);

    		data.dbcFile.setData(col, row, tmpi);
    	}
    	else if(types[col]==dbc.COL_TYPE_BOOLEAN){
    		int tmpi;
    		if(Boolean.valueOf(value.toString())){
    			tmpi=1;
    		}
    		else tmpi=0;
    		data.dbcFile.setData(col, row, tmpi);
    	}
    	else {
    		data.dbcFile.setData(col, row, Integer.parseInt(value.toString()) );
    	}
    	fireTableCellUpdated(row, col);/**/
    }
}