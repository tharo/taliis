package de.taliis.plugins.dialogs;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.InvalidClassException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Properties;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.filechooser.FileFilter;

import starlight.taliis.core.chunks.ChunkNotFoundException;
import starlight.taliis.core.files.wowfile;

import de.taliis.editor.configMananger;
import de.taliis.editor.fileMananger;
import de.taliis.editor.openedFile;
import de.taliis.editor.plugin.*;

/**
 * This is a smal plugin that was made to demonstrate 
 * some basic things.
 * 
 * - How to create some menue entrys
 * - How to load some icons from plugins own 
 * - Shows how to deal with the fileMananger
 * - use the global config file
 * 
 * @author Tharo Herberg
 *
 */
public class fileManagementDialogs
	implements Plugin, ActionListener 
{
	ImageIcon iNew, iOpen, iClose, iSave, iSaveAs;
	JMenuItem mNew, mOpen, mClose, mSave, mSaveAs,
				mLastFiles[];
	JMenu mainMenu;
	
	Vector<Plugin>ppool;
	fileMananger fm;
	eventServer es;
	configMananger cm;
	
	String lastTenFiles[];
	
	private final static String MY_CONF_STRING = "taliis_SwingSysDialog_";
	private final static int LAST_FILES_COUNT = 5;
	
	public int getPluginType() {
		return PLUGIN_TYPE_FUNCTION;
	}

	
	public void setClassLoaderRef(URLClassLoader ref) {
		URL u;
		try {
			u = ref.getResource("icons/page.png");
			iNew = new ImageIcon( u );
			
			u = ref.getResource("icons/folder_go.png");
			iOpen = new ImageIcon( u );
			
			u = ref.getResource("icons/folder.png");
			iClose = new ImageIcon( u );
			
			u = ref.getResource("icons/disk.png");
			iSave = new ImageIcon( u );
			
			u = ref.getResource("icons/disk_multiple.png");
			iSaveAs = new ImageIcon( u );
		} catch(NullPointerException e) {}
	}

	
	public void setFileManangerRef(fileMananger ref) {
		fm = ref;
	}
	
	
	public void setEventServer(eventServer ref) {
		es = ref;
	}
	
	 
	public void setConfigManangerRef(configMananger ref) {
		cm = ref;
		
		// get last 10 files
		lastTenFiles = new String[10];
		Properties p = cm.getGlobalConfig();
		for(int c=0; c<10; c++)
			lastTenFiles[c] = p.getProperty(MY_CONF_STRING+"lastfile_"+c);
	}
	
	
	
	public void setMenuRef(JMenuBar ref) {
		mNew = new JMenuItem("New ..");
		mNew.addActionListener(this);
		mNew.setIcon(iNew);
		
		mOpen = new JMenuItem("Open");
		mOpen.addActionListener(this);
		mOpen.setIcon(iOpen);
		mOpen.setAccelerator(KeyStroke.getKeyStroke(
		        KeyEvent.VK_O, ActionEvent.CTRL_MASK));
		
		mClose = new JMenuItem("Close");
		mClose.addActionListener(this);
		mClose.setIcon(iClose);
		mClose.setAccelerator(KeyStroke.getKeyStroke(
		        KeyEvent.VK_C, ActionEvent.CTRL_MASK));
		
		mSave = new JMenuItem("Save");
		mSave.addActionListener(this);
		mSave.setIcon(iSave);
		mSave.setAccelerator(KeyStroke.getKeyStroke(
		        KeyEvent.VK_S, ActionEvent.CTRL_MASK));
		
		mSaveAs = new JMenuItem("Save As..");
		mSaveAs.addActionListener(this);
		mSaveAs.setIcon(iSaveAs);
		mSaveAs.setAccelerator(KeyStroke.getKeyStroke(
		        KeyEvent.VK_S, ActionEvent.CTRL_MASK + ActionEvent.SHIFT_MASK));
		
		// opened files
		mLastFiles = new JMenuItem[5];
		 for(int c=0; c<5; c++) {
			if(lastTenFiles[c]==null) {
				mLastFiles[c] = new JMenuItem();
				mLastFiles[c].setVisible(false);
			}
			else mLastFiles[c] = new JMenuItem((c+1) + ". " + lastTenFiles[c]);
			mLastFiles[c].setName("file_history_"+c);
			mLastFiles[c].addActionListener(this);
			mLastFiles[c].validate();
		}
		mainMenu = ref.getMenu(0);
		mainMenu.insertSeparator(0);
		
		for(int c=4; c>=0; c--)
			if(mLastFiles[c]!=null)
				mainMenu.add(mLastFiles[c], 0);
		
		mainMenu.insertSeparator(0);
		mainMenu.add(mSaveAs, 0);
		mainMenu.add(mSave, 0);
		mainMenu.insertSeparator(0);
		mainMenu.insert(mClose, 0);
		mainMenu.add(mOpen, 0);
		mainMenu.insertSeparator(0);
		mainMenu.add(mNew, 0);
	}	
	
	public void unload() {		
		Properties p = cm.getGlobalConfig();
		// save opened files?
		
		// save last 10 opened files
		for(int c=0; c<10; c++)
			if(lastTenFiles[c]!=null)
				p.setProperty(MY_CONF_STRING+"lastfile_"+c, lastTenFiles[c]);
		
		// kill stuff
	}
	
	public void setPluginPool(Vector<Plugin> ref) { 
		ppool = ref;
	}	
	
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==mNew) {
			menNew();
		}
		
		else if(e.getSource()==mOpen) {
			try {
				menOpen();
			} catch (InvalidClassException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (ChunkNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
		else if(e.getSource()==mClose) {
			if(fm.getActiveFile()==null) return;
			
			fm.closeFile( fm.getActiveFile() );
			es.updateTable();
		}
		
		else if(e.getSource()==mSave) {
			if(fm.getActiveFile()==null) return;
			
			openedFile of = fm.getActiveFile();
			
			// do we need to detect a (new) file name?
			if(of.f==null) {
				menSaveAs();
			}
			else if(of.f.canWrite()) {
				menSaveAs();
			}
			else fm.saveFile( of );
		}
		
		else if(e.getSource()==mSaveAs) {
			menSaveAs();
		}
		else {
			// other menu entry?
			if(e.getSource() instanceof JMenuItem) {
				JMenuItem tmp = (JMenuItem)e.getSource();
				if(tmp.getName().indexOf("file_history_")!=-1) {
					//int num = new Integer(tmp.getName().substring(13, 14));
					//System.out.println(num);
					System.out.println("Try to open:" + tmp.getText().substring(3));
					
					File f = new File(tmp.getText().substring(3));
					if(f.exists()==false) {
						tmp.setEnabled(false);
						return;
					}
		
					// try to open the file
					try {
						fm.openFile( f );
					} catch(Exception ne) {
						return;
					}
					

					es.updateTable();
						
					update(f.getPath());
				}
			}
		}
	}

	 public ImageIcon getIcon() { return null; }
	 public boolean checkDependencies() { return true; }
	 public String[] getSupportedDataTypes() {	return new String[]{ }; }
	 public String[] neededDependencies() { return null; }

	 /**
	  * Whenever we did something with a file: update
	  * @param fileName
	  */
	 private void update(String fileName) {
		 pushFile(fileName);
		 setLastPath(fileName);
		 updateMenHistory(); 
	 }
	 
	 /**
	  * Filles / updates the last files menu entrys
	  */
	 private void updateMenHistory() {
		 for(int c=0; c<5; c++) {
			if(lastTenFiles[c]==null) break;
			mLastFiles[c].setText((c+1) + ". " + lastTenFiles[c]);
			mLastFiles[c].setVisible(true);
		 }
	 }
	 
	 /**
	  * Adds a new latest file in our history of the last
	  * opened 10 files
	  * @param latestFile
	  */
	 private void pushFile(String latestFile) {
		 String nLTF[] = new String[10];
		 nLTF[0] = latestFile;
		 
		 int put = 1;
		 for(int c=0; c<9; c++) {
			 // file allready in out list?
			 if(lastTenFiles[c]==null) break;
			 if(lastTenFiles[c].compareTo(latestFile)!=0) {
				 nLTF[put++] = lastTenFiles[c];
			 }
		 }
		 
		 lastTenFiles = nLTF;
	 }
	 
	 /**
	  * Saves the last used path
	  * @param path
	  */
	 private void setLastPath(String path) {
		 cm.getGlobalConfig().setProperty(MY_CONF_STRING+"lastdir", path);
	 }
	 
	 /**
	  * The new dialog
	  */
	 private int menNew() {
			Vector <ppair> choices = new Vector<ppair>();
			
			for(Plugin p : ppool) {
				if(p instanceof PluginStorage) {
					for(String s : p.getSupportedDataTypes())
						choices.add(new ppair(s,(PluginStorage)p));
				}
			}
			
			if(choices.size()==0) return -1;
			
			ppair result = (ppair) JOptionPane.showInputDialog(
					mNew.getComponent(),
                    "Chose a File type:\n",
                    "New ..",
                    JOptionPane.PLAIN_MESSAGE,
                    null,
                    choices.toArray(),
                    null);
			if(result==null) return -2;
			
			PluginStorage n = result.ref;
			wowfile tmp = n.create();
			
			// register only at empty return. maybe
			// the plugin want to register it itself
			if(tmp!=null) {
				fm.registerObject(tmp, null);
				es.updateTable();
			}
			return 1;
	 }	
	 
	 /**
	  * The open dialog
	 * @throws ChunkNotFoundException 
	 * @throws InvalidClassException 
	  */
	 private int menOpen() throws InvalidClassException, ChunkNotFoundException {
		JFileChooser fc = new JFileChooser();
			
		// last opened dir?
		String dir = cm.getGlobalConfig().getProperty(
				MY_CONF_STRING + "lastdir"
			);
		if(dir!=null)
			fc.setCurrentDirectory(new File(dir));
	
		// filter?
		for(Plugin p : ppool) {
			if(p instanceof PluginStorage) {
				FileFilter tmp = ((PluginStorage)p).getFiter();
				if(tmp!=null) fc.addChoosableFileFilter( tmp );
			}
		}

		fc.setFileFilter( fc.getAcceptAllFileFilter() );
		fc.showOpenDialog(null);
		
		if(fc.getSelectedFile()==null) return -1;
		try { 
			fm.openFile( fc.getSelectedFile() );
		} catch(Exception e) {
			return -2;
		}
			
		es.updateTable();
		
		// save latest dir
		update(fc.getSelectedFile().getPath());
		
		return 1;
	 }
	 
	 
	 /**
	  * The save as dialog
	  */
	 private int menSaveAs() {
		if(fm.getActiveFile()==null) return -1;
		
		JFileChooser fc = new JFileChooser();
		openedFile s = fm.getActiveFile();
		if(s.f!=null)
			fc.setCurrentDirectory( s.f );
		fc.showSaveDialog( null );
			
		if(fc.getSelectedFile()==null) return -2;
		s.f = fc.getSelectedFile();
			
		fm.saveFile( s );
		es.updateTable();
			
		// save latest dir
		update(fc.getSelectedFile().getPath());
		
		return 1;
	 }
	 
	 class ppair {
		String name;
		PluginStorage ref;
		
		public ppair(String n, PluginStorage r) {
			name = n;
			ref = r;
		}
		public String toString() { 
			return name;
		};
	}
}