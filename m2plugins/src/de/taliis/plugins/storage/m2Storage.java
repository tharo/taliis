package de.taliis.plugins.storage;

import java.io.File;
import java.io.InvalidClassException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JMenuBar;
import javax.swing.filechooser.FileFilter;

import starlight.taliis.core.files.wdt;
import starlight.taliis.core.files.wowfile;
import starlight.taliis.core.files.m2;
import starlight.taliis.helpers.fileLoader;
import de.taliis.editor.configMananger;
import de.taliis.editor.fileMananger;
import de.taliis.editor.openedFile;
import de.taliis.editor.plugin.Plugin;
import de.taliis.editor.plugin.PluginStorage;
import de.taliis.editor.plugin.eventServer;

/**
 * Storage thingy for m2s o.O
 * 
 * 
 * @author Tigurius
 *
 */



public class m2Storage implements Plugin, PluginStorage {

	ImageIcon icon = null;
	fileMananger fm;
	eventServer es;
	
	@Override
	public boolean checkDependencies() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public ImageIcon getIcon() {
		// TODO Auto-generated method stub
		return icon;
	}

	@Override
	public int getPluginType() {
		// TODO Auto-generated method stub
		return this.PLUGIN_TYPE_STORAGE;
	}

	@Override
	public String[] getSupportedDataTypes() {
		// TODO Auto-generated method stub
		return new String[]{ "m2" }; 
	}

	@Override
	public String[] neededDependencies() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setClassLoaderRef(URLClassLoader ref) {
		// TODO Auto-generated method stub
		try {
			URL u = ref.getResource("icons/shading.png");
			icon = new ImageIcon( u );
		} catch(NullPointerException e) {}	
	}

	@Override
	public void setConfigManangerRef(configMananger ref) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEventServer(eventServer ref) {
		// TODO Auto-generated method stub
		es = ref;
	}

	@Override
	public void setFileManangerRef(fileMananger ref) {
		// TODO Auto-generated method stub
		fm = ref;
	}

	@Override
	public void setMenuRef(JMenuBar ref) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setPluginPool(Vector<Plugin> ref) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void unload() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public wowfile create() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public FileFilter getFiter() {
		// TODO Auto-generated method stub
		return new FileFilter() {

			public boolean accept(File arg0) {
				if(arg0.isDirectory())return true;
				if(arg0.toString().toLowerCase().endsWith(".m2")) return true;
				else return false;
			}

			public String getDescription() {
				return "WOW Model Files";
			}
		};
	}

	@Override
	public wowfile load(File f) throws InvalidClassException {
		// TODO Auto-generated method stub
		return new m2( fileLoader.openBuffer(f.getAbsolutePath()) );
	}

	@Override
	public int save(openedFile file) {
		if(file.obj instanceof wowfile) {
			wowfile o = (wowfile)file.obj;
			o.render();
			fileLoader.saveBuffer(o.buff , file.f.getAbsolutePath());
			return 1;
		}
		return -1;
	}
	
}